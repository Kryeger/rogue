import Chance from "chance";
import _ from "lodash";
import { Map } from "rot-js";
import Digger from "rot-js/lib/map/digger";
import DividedMaze from "rot-js/lib/map/dividedmaze";
import EllerMaze from "rot-js/lib/map/ellermaze";
import IceyMaze from "rot-js/lib/map/iceymaze";
import { Vector3 } from "../interfaces/positions";
import { Floor, FloorInfo, TargetType, WorldDifficulty } from "../shared_internal";

const chance = new Chance();

export interface GameMapInfo {
    floors: Array<FloorInfo>;
    width: number;
    height: number;
}

export default class GameMap {
    private readonly _width: number;
    private readonly _height: number;
    private readonly _floors: Array<Floor> = [];
    //TODO: maybe optimize to only have 1 instance of eacah generator appwide
    private readonly _digger: Digger;
    private readonly _dividedMaze: DividedMaze;
    private readonly _ellerMaze: EllerMaze;
    private readonly _iceyMaze: IceyMaze;
    private readonly _difficulty: WorldDifficulty;

    constructor(width: number, height: number, difficulty: WorldDifficulty) {
        this._width = width;
        this._height = height;
        this._difficulty = difficulty;

        this._digger = new Map.Digger(width, height);
        this._dividedMaze = new Map.DividedMaze(width, height);
        this._ellerMaze = new Map.EllerMaze(width, height);
        this._iceyMaze = new Map.IceyMaze(width, height);

        this.generateFloors(1);
    }

    get difficulty(): WorldDifficulty {
        return this._difficulty;
    }

    get floors(): Array<Floor> {
        return this._floors;
    }

    get width(): number {
        return this._width;
    }

    get height(): number {
        return this._height;
    }

    get digger(): Digger {
        return this._digger;
    }

    get dividedMaze(): DividedMaze {
        return this._dividedMaze;
    }

    get ellerMaze(): EllerMaze {
        return this._ellerMaze;
    }

    get iceyMaze(): IceyMaze {
        return this._iceyMaze;
    }

    //TODO: global loot quality var

    generateFloors = (count: number) => {
        _.range(0, count).forEach(el =>
            this.floors.push(
                new Floor(
                    this.floors.length,
                    this.width,
                    this.height,
                    this.difficulty,
                    chance.pickone([
                        this.digger /*, this.dividedMaze, this.iceyMaze, this.ellerMaze*/,
                    ])
                )
            )
        );
    };

    checkMove = ({ x, y, z }: Vector3) => {
        return this.floors[z]?.checkMove({ x, y });
    };

    checkTarget = ({ x, y, z }: Vector3, targetType: TargetType) => {
        return this.floors[z]?.checkTarget({ x, y }, targetType);
    };

    async getInfo(): Promise<GameMapInfo> {
        const { width, height } = this;
        return {
            width,
            height,
            floors: await Promise.all(this.floors.map(floor => floor.getInfo())),
        };
    }
}
