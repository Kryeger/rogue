import Chance from "chance";
import { Simulate } from "react-dom/test-utils";
import { Path } from "rot-js";
import shortUUID from "short-uuid";
import Server from "../../server/class/Server";
import {
    Character,
    CharacterInfo,
    CharacterManager,
    Enemy,
    EnemyInfo,
    EnemyManager,
    Item,
    ItemInfo,
    ItemManager,
    Player,
    PlayerManager,
    Tile,
    TileInfo,
} from "../../server/server_internal";
import { diagonalDistance, doIfChance, raycast } from "../../server/utils/Utils";
import { Maybe } from "../interfaces/Maybe";
import { Vector2 } from "../interfaces/positions";
import { SMsg } from "../interfaces/SMsg";
import { WorldDifficulty } from "../shared_internal";
import { TileTypes } from "../TileTypes";
import play = Simulate.play;

export enum TargetType {
    Character,
    Enemy,
}

export interface FloorInfo {
    floorCount: number;

    matrix: Array<Array<Maybe<TileInfo>>>;

    spawnPosition: Vector2;
    leavePosition: Vector2;

    players: Array<Player>;
    items: Array<ItemInfo>;
    characters: Array<CharacterInfo>;
    enemies: Array<EnemyInfo>;
}

const chance = new Chance();

export default class Floor {
    static config = {
        tickSpeed: 200,
    };
    private readonly _uuid: string;
    private readonly _floorCount: number;
    private readonly _matrix: Array<Array<Maybe<Tile>>>;
    private readonly _width: number;
    private readonly _height: number;
    private readonly _spawnPosition: Vector2 = { x: -1, y: -1 };
    private readonly _leavePosition: Vector2 = { x: -1, y: -1 };
    private readonly _players: Array<string> = [];
    private readonly _items: Array<string> = [];
    private readonly _characters: Array<string> = [];
    private readonly _enemies: Array<string> = [];
    private readonly _difficulty: WorldDifficulty;
    private _interval: NodeJS.Timeout;

    constructor(
        floorCount: number,
        width: number,
        height: number,
        difficulty: WorldDifficulty,
        generator: any
    ) {
        this._uuid = shortUUID().generate();
        this._floorCount = floorCount;
        this._width = width;
        this._height = height;
        this._difficulty = difficulty;

        this._matrix = Array(width)
            .fill(undefined)
            .map(() => Array(height).fill(undefined));

        generator.create((x: number, y: number, value: number) => {
            this._matrix[x][y] = value ? new Tile(TileTypes.Wall) : new Tile(TileTypes.Floor);
        });

        while (this.spawnPosition.x === -1 || this.spawnPosition.y === -1) {
            const [x, y] = [
                chance.integer({ min: 0, max: this.width - 1 }),
                chance.integer({ min: 0, max: this.height - 1 }),
            ];

            if (!this.matrix[x][y]?.tileTypeInfo.solid) this._spawnPosition = { x, y };
        }

        while (this.leavePosition.x === -1 || this.leavePosition.y === -1) {
            const [x, y] = [
                chance.integer({ min: 0, max: this.width - 1 }),
                chance.integer({ min: 0, max: this.height - 1 }),
            ];

            if (
                !this.matrix[x][y]?.tileTypeInfo.solid &&
                (this.spawnPosition.x !== x || this.spawnPosition.y !== y)
            )
                this._leavePosition = { x, y };
        }

        this.spawnItems(5, 40);
        this.spawnEnemies(1, 100);

        this._interval = setInterval(this.tick, Floor.config.tickSpeed);
    }

    get difficulty(): WorldDifficulty {
        return this._difficulty;
    }

    get floorCount(): number {
        return this._floorCount;
    }

    get uuid(): string {
        return this._uuid;
    }

    get characters(): Array<string> {
        return this._characters;
    }

    get enemies(): Array<string> {
        return this._enemies;
    }

    get players(): Array<string> {
        return this._players;
    }

    get items(): Array<string> {
        return this._items;
    }

    get spawnPosition(): Vector2 {
        return this._spawnPosition;
    }

    get leavePosition(): Vector2 {
        return this._leavePosition;
    }

    get width(): number {
        return this._width;
    }

    get height(): number {
        return this._height;
    }

    get matrix(): Array<Array<Maybe<Tile>>> {
        return this._matrix;
    }

    tick = async () => {
        if (!this.characters.length) return;

        this.moveEnemies();

        Server.io.to(this.uuid).emit(SMsg.MAP_UPDATE_RES, {
            success: true,
            enemies: await this?.getResolvedEnemies()?.map(el => el.getInfo()),
        });
    };

    spawnItems = (min: number, max: number) => {
        const maxItemCount = chance.integer({ min, max });
        let itemCount = 0;

        this.iterate((i, j, tile) => {
            if (tile?.tileTypeInfo.solid || itemCount > maxItemCount) return;

            //This will spawn an item 5 times out of 100.
            const success = this.spawnItem({ x: i, y: j }, 0.05);

            if (success) {
                itemCount++;
            }
        });
    };

    spawnItem = (position: Vector2, chanceToSpawn: number) => {
        const newItem = ItemManager.createByChance(position, chanceToSpawn, this.difficulty);

        if (newItem) {
            this.insertItem(newItem.uuid);
            return true;
        }

        return false;
    };

    spawnEnemies = (min: number, max: number) => {
        const maxEnemyCount = chance.integer({ min, max });
        let enemyCount = 0;

        this.iterate((i, j, tile) => {
            if (tile?.tileTypeInfo.solid || enemyCount >= maxEnemyCount) return;

            const newEnemy = EnemyManager.createByChance(
                "Enemy",
                this.floorCount + 1,
                this.difficulty,
                { x: i, y: j },
                0.05
            );

            if (newEnemy) {
                this.insertEnemy(newEnemy.uuid);
                enemyCount++;
            }
        });
    };

    iterate = (iterateFn: (x: number, y: number, item: Maybe<Tile>) => any) => {
        this._matrix.forEach((row, i) => row.forEach((item, j) => iterateFn(i, j, item)));
    };

    checkMove = ({ x, y }: Vector2, isEnemy?: boolean) => {
        const enemyInTheWay = this.enemies.find(el => {
            const enemy = EnemyManager.get(el);
            return enemy?.position.x === x && enemy?.position.y === y;
        });

        const charInTheWay = this.characters.find(el => {
            const char = CharacterManager.get(el);
            return char?.position.x === x && char?.position.y === y;
        });

        return (
            x >= 0 &&
            y >= 0 &&
            x < this._width &&
            y < this._height &&
            !this.matrix[x][y]?.tileTypeInfo?.solid &&
            (isEnemy || !enemyInTheWay) &&
            (isEnemy || !charInTheWay)
        );
    };

    checkTarget = ({ x, y }: Vector2, targetType: TargetType) => {
        switch (targetType) {
            case TargetType.Character:
                return this.getResolvedCharacters().find(
                    el => el.position.x === x && el.position.y === y
                );
            case TargetType.Enemy:
                return this.getResolvedEnemies().find(
                    el => el.position.x === x && el.position.y === y
                );
        }
    };

    insertItem = (uuid: string) => {
        this.items.push(uuid);
    };

    removeItem = (uuid: string) => {
        const index = this.items.indexOf(uuid);

        if (index !== -1) {
            this.items.splice(index, 1);
            return true;
        }

        return false;
    };

    insertEnemy = (uuid: string) => {
        this.enemies.push(uuid);
    };

    removeEnemy = (uuid: string) => {
        this.enemies.splice(this.enemies.indexOf(uuid), 1);
    };

    getItemsByPosition = (position: Vector2) => {
        return this.getResolvedItems().filter(
            item => item?.position?.x === position.x && item?.position.y === position.y
        );
    };

    checkRaycast = (a: Vector2, b: Vector2) => {
        const points = raycast(a, b);

        for (let point of points) {
            if (this.matrix[point.x][point.y]?.tileTypeInfo.solid) {
                return;
            }
        }

        return points;
    };

    moveEnemies = () => {
        this.enemies.forEach(enemyUuid => {
            //should exist.
            const enemy = EnemyManager.get(enemyUuid)!;

            if (enemy.timeToForget < 0) {
                enemy.timeToForget = 50;
                enemy.targetCharUuid = undefined;
            }

            doIfChance(enemy.targetCharUuid ? 1 : 0.25, () => {
                //If the current enemy is doesn't have a target
                if (!enemy.targetCharUuid) {
                    const resolvedChars = this.getResolvedCharacters();

                    //Find characters that are closer than 10 tiles but which are also visbile
                    //(i.e a raycast between the enemy and the potential target doesn't pass through a solid block
                    const nearbyChars = resolvedChars.filter(char => {
                        if (!char) return false;
                        return (
                            diagonalDistance(enemy.position, char.position) < 10 &&
                            !!this.checkRaycast(enemy.position, char.position)
                        );
                    });

                    //If there are any nearby characters
                    if (nearbyChars.length) {
                        //Set the enemies target as one of the nearby characters (at random).
                        enemy.targetCharUuid = chance.pickone(nearbyChars).uuid;
                    }
                }

                const targetChar =
                    enemy.targetCharUuid && CharacterManager.get(enemy.targetCharUuid)!;

                //If the enemy has a target
                if (targetChar) {
                    const dijkstra = new Path.Dijkstra(
                        //Starting position
                        enemy.position.x,
                        enemy.position.y,
                        //For a given position, this function will decide if the tile can be passed through.
                        (x, y) => !this.matrix[x][y]?.tileTypeInfo.solid,
                        //Only up/down/left/right movement is allowed.
                        { topology: 4 }
                    );

                    let steps: Array<Vector2> = [];

                    //For every step towards the target character's position, push the step in the array.
                    dijkstra.compute(targetChar.position.x, targetChar.position.y, (x, y) => {
                        steps.push({ x, y });
                    });

                    //If there is a second to last step and is valid, then move the enemy.
                    steps[steps.length - 2] &&
                        this.checkMove(steps[steps.length - 2]) &&
                        (enemy.position = steps[steps.length - 2]);
                }

                enemy.timeToForget--;
            });

            doIfChance(enemy.targetCharUuid ? 0 : 1, () => {
                const move = this.getRandomEnemyMove(enemy.position);

                if (move) {
                    enemy.position.x = move.x;
                    enemy.position.y = move.y;
                }
            });
        });
    };

    getRandomEnemyMove = (position: Vector2) => {
        const { x, y } = position;

        const possibleMoves = [
            position,
            { x: x + 1, y },
            { x: x - 1, y },
            { x, y: y + 1 },
            { x, y: y - 1 },
        ].filter(el => this.checkMove(el));

        if (!possibleMoves.length) return null;

        const choice = chance.pickone(possibleMoves);

        if (!this.checkMove(choice)) return null;

        return choice;
    };

    insertPlayer = async (uuid: string) => {
        this.players.push(uuid);

        const player = await PlayerManager.get(uuid);

        if (!player) return;
        const alreadyExists = CharacterManager.get(player.charUuid);

        const { spawnPosition, leavePosition } = this;

        if (alreadyExists) {
            if (alreadyExists.position.z > this.floorCount) {
                alreadyExists.position.x = leavePosition.x;
                alreadyExists.position.y = leavePosition.y;
            } else {
                alreadyExists.position.x = spawnPosition.x;
                alreadyExists.position.y = spawnPosition.y;
            }

            if (alreadyExists.position.z !== this.floorCount) {
                this.characters.push(alreadyExists.uuid);
            }

            alreadyExists.position.z = this.floorCount;
        }

        if (!alreadyExists) {
            const newCharacter = await CharacterManager.create(
                this.floorCount,
                spawnPosition.x,
                spawnPosition.y
            );
            this.characters.push(newCharacter.uuid);
            return newCharacter.uuid;
        }

        return alreadyExists.uuid;
    };

    removePlayer = async (uuid: string) => {
        const index = this.players.indexOf(uuid);
        if (index === -1) return false;

        const player = await PlayerManager.get(uuid);
        if (!player || !player.charUuid) return false;

        const character = await CharacterManager.get(player?.charUuid);
        if (!character) return false;

        const charIndex = this.characters.indexOf(character.uuid);
        if (charIndex === -1) return false;

        this.players.splice(index, 1);
        this.characters.splice(charIndex, 1);
        return true;
    };

    getResolvedCharacters = (): Array<Character> => {
        return this.characters
            .map(charUuid => CharacterManager.get(charUuid))
            .filter(el => !!el) as Array<Character>;
    };

    getResolvedItems = () => {
        return this.items
            .map(itemUuid => ItemManager.get(itemUuid))
            .filter(el => !!el) as Array<Item>;
    };

    getResolvedEnemies = () => {
        return this.enemies
            .map(enemyUuid => EnemyManager.get(enemyUuid))
            .filter(el => !!el) as Array<Enemy>;
    };

    async getInfo(): Promise<FloorInfo> {
        //Create a new matrix.
        const matrixInfo = Array(this.width)
            .fill(undefined)
            .map(() => Array(this.height).fill(undefined));

        //Fill it with the information of each tile.
        this.iterate((i, j, tile) => (matrixInfo[i][j] = tile?.getInfo()));

        const { players, floorCount, spawnPosition, leavePosition } = this;
        const resolvedPlayers = (
            await Promise.all(
                //Resolve each player in the players array and get their info.
                (await PlayerManager.getMany(players)).map(playerDoc => playerDoc?.getInfo())
            )
        )
            //Filter the result to exclude possible undefined values.
            .filter(el => !!el) as Array<Player>;

        //Resolve each character, item and enemy and fetch their info.
        const characters = this.getResolvedCharacters().map(char => char.getInfo());
        const items = this.getResolvedItems()
            .map(item => item?.getInfo())
            .filter(el => !!el) as Array<ItemInfo>;
        const enemies = this.getResolvedEnemies().map(enemy => enemy.getInfo());

        //Return an object containing all the info.

        return {
            matrix: matrixInfo,
            players: resolvedPlayers,
            characters,
            items,
            enemies,
            floorCount,
            spawnPosition,
            leavePosition,
        };
    }
}
