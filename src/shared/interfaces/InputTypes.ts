export enum InputTypes {
    INPUT_UP = "input_up",
    INPUT_RIGHT = "input_right",
    INPUT_DOWN = "input_down",
    INPUT_LEFT = "input_left",
    INPUT_ACTION = "input_action",
}
