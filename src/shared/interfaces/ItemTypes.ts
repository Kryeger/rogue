import { TileTypeInfo, TileType } from "../TileTypes";
import { ItemAttribute, ItemAttributeModifier, ItemAttributes } from "./ItemAttributes";

export enum ItemCategoryType {
    Weapon = "itemCategoryTypeWeapon",
    Armor = "itemCategoryTypeWeapon",
    Potion = "itemCategoryTypePotion",
}

export enum ItemType {
    Sword = "itemTypeSword",
    Knife = "itemTypeKnife",
    Helmet = "itemTypeHelmet",
    Chestplate = "itemTypeChestplate",
    Pants = "itemTypePants",
    Boots = "itemTypeBoots",
    HealingPotion = "itemTypeHealingPotion",
}

export const ItemTypeToEquipmentSlot = {
    [ItemType.Sword]: "weapon",
    [ItemType.Knife]: "weapon",
    [ItemType.Helmet]: "helmet",
    [ItemType.Chestplate]: "chestplate",
    [ItemType.Pants]: "pants",
    [ItemType.Boots]: "boots",
} as { [key in ItemType]: string };

export interface ItemTypeInfo {
    name: string;
    category: ItemCategoryType;
    attributes: Array<ItemAttributeModifier>;
}

export const ItemTypes = {
    [ItemType.Sword]: {
        name: "Sword",
        category: ItemCategoryType.Weapon,
        attributes: [
            //Swords have a 1.75 multiplier on their Damage attribute.
            { attribute: ItemAttribute.Damage, bias: 1.75 },
            //and a 1.2 multiplier on their Speed attribute.
            { attribute: ItemAttribute.Speed, bias: 1.2 },
        ],
    },
    [ItemType.Knife]: {
        name: "Knife",
        category: ItemCategoryType.Weapon,
        attributes: [
            { attribute: ItemAttribute.Damage, bias: 1.4 },
            { attribute: ItemAttribute.Speed, bias: 1.6 },
        ],
    },
    [ItemType.Helmet]: {
        name: "Helmet",
        category: ItemCategoryType.Armor,
        attributes: [
            { attribute: ItemAttribute.Defense, bias: 1.3 },
            { attribute: ItemAttribute.Speed, bias: 0.9 },
        ],
    },
    [ItemType.Chestplate]: {
        name: "Chestplate",
        category: ItemCategoryType.Armor,
        attributes: [
            { attribute: ItemAttribute.Defense, bias: 1.6 },
            { attribute: ItemAttribute.Speed, bias: 0.7 },
        ],
    },
    [ItemType.Pants]: {
        name: "Pants",
        category: ItemCategoryType.Armor,
        attributes: [
            { attribute: ItemAttribute.Defense, bias: 0.35 },
            { attribute: ItemAttribute.Speed, bias: 0.8 },
        ],
    },
    [ItemType.Boots]: {
        name: "Boots",
        category: ItemCategoryType.Armor,
        attributes: [
            { attribute: ItemAttribute.Defense, bias: 0.2 },
            { attribute: ItemAttribute.Speed, bias: 0.95 },
        ],
    },
    [ItemType.HealingPotion]: {
        name: "Healing Potion",
        category: ItemCategoryType.Potion,
        attributes: [{ attribute: ItemAttribute.Heal, bias: 1 }],
    },
} as { [key in ItemType]: ItemTypeInfo };

export interface ItemCategoryTypeInfo {
    nameModifiers: Array<string>;
}

export const ItemCategoryTypes = {
    [ItemCategoryType.Weapon]: {
        nameModifiers: ["Long", "Short", "Heavy"],
    },
    [ItemCategoryType.Armor]: {
        nameModifiers: ["Light", "Heavy"],
    },
    [ItemCategoryType.Potion]: {
        nameModifiers: ["Small", "Big"],
    },
} as { [key in ItemCategoryType]: ItemCategoryTypeInfo };
