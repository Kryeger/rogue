export interface Auth {
    uuid: string;
    secret: string;
    socketId: string;
    disconnectTimeout?: NodeJS.Timeout;
}
