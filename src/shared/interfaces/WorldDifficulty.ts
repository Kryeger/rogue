export enum WorldDifficulty {
    Easy = "worldDifficultyEasy",
    Medium = "worldDifficultyMedium",
    Hard = "worldDifficultyHard",
    Extreme = "worldDifficultyExtreme",
}

export const WorldDifficulties = {
    [WorldDifficulty.Easy]: { label: "Easy", color: "#39f54f", bias: 1 },
    [WorldDifficulty.Medium]: { label: "Medium", color: "#faf146", bias: 1.2 },
    [WorldDifficulty.Hard]: { label: "Hard", color: "#fc9242", bias: 1.5 },
    [WorldDifficulty.Extreme]: { label: "Extreme", color: "#fd5959", bias: 2 },
};
