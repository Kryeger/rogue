import { Colors } from "../../client/other/Colors";

export enum RarityType {
    Common = "rarityCommon",
    Uncommon = "rarityUncommon",
    Rare = "rarityRare",
    Epic = "rarityEpic",
    Legendary = "rarityLegendary",
}

export interface RarityTypeInfo {
    name: string;
    color: string;
    statsBias: number;
    priceBias: number;
}

export const RarityTypes = {
    [RarityType.Common]: {
        name: "Common",
        color: Colors.CommonGray,
        statsBias: 1,
        priceBias: 1,
    },

    [RarityType.Uncommon]: {
        name: "Uncommon",
        color: Colors.UncommonBlue,
        statsBias: 1.2,
        priceBias: 1.2,
    },

    [RarityType.Rare]: {
        name: "Rare",
        color: Colors.RarePurple,
        statsBias: 1.4,
        priceBias: 1.6,
    },

    [RarityType.Epic]: {
        name: "Epic",
        color: Colors.EpicPink,
        statsBias: 1.6,
        priceBias: 2,
    },

    [RarityType.Legendary]: {
        name: "Legendary",
        color: Colors.LegendaryOrange,
        statsBias: 1.8,
        priceBias: 4,
    },
} as { [key in RarityType]: RarityTypeInfo };
