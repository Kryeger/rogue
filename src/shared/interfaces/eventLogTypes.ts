export enum EventLogMessageType {
    Normal,
    Attack,
    Join,
    Leave,
}

export interface EventLogMessage {
    type: EventLogMessageType;
    timestamp: number;
    message: Array<{ text: string; color?: string }>;
}
