import { Socket } from "socket.io";

export type ServerAction = (data: any, socket?: Socket) => any;
