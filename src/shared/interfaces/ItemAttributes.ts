export enum ItemAttribute {
    Damage = "itemAttributeDamage",
    Defense = "itemAttributeDefense",
    Speed = "itemAttributeSpeed",
    Heal = "itemAttributeHeal",
}

export interface ItemAttributeInfo {
    name: string;
    acronym: string;
    color: string;
}

export const ItemAttributes = {
    [ItemAttribute.Damage]: {
        name: "Damage",
        acronym: "DMG",
        color: "#f35712",
    },
    [ItemAttribute.Defense]: {
        name: "Defense",
        acronym: "DEF",
        color: "#2138d0",
    },
    [ItemAttribute.Speed]: {
        name: "Speed",
        acronym: "SPD",
        color: "#ffdc2b",
    },
    [ItemAttribute.Heal]: {
        name: "Heal",
        acronym: "HEAL",
        color: "#3e9d20",
    },
} as { [key in ItemAttribute]: ItemAttributeInfo };

export interface ItemAttributeModifier {
    attribute: ItemAttribute;
    bias: number;
}

export interface ItemAttributeInstance {
    attribute: ItemAttribute;
    value: number;
}
