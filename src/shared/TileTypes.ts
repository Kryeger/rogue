export interface TileTypeInfo {
    name: string;
    solid: boolean;
    char: string;
}

export enum TileType {
    Floor = "tileTypeFloor",
    Player = "tileTypePlayer",
    Wall = "tileTypeWall",
    StairsUp = "tileTypeStairsUp",
    StairsDown = "tileTypeStairsDown",
}

export const TileTypes = {
    Floor: { name: "Floor", solid: false, char: " " },
    Player: { name: "Player", solid: true, char: "@" },
    Wall: { name: "Wall", solid: true, char: "#" },
    StairsUp: { name: "Stairs Up", solid: false, char: "˄" },
    StairsDown: { name: "Stairs Down", solid: false, char: "˅" },
} as { [key in keyof typeof TileType]: TileTypeInfo };
