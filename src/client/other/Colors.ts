export const Colors = {
    Black: "#000000",
    White: "#ffffff",
    InertGray: "#AAAAAA",
    ItemGray: "#969696",
    PlayerGreen: "#3a9e00",
    CommonGray: "#d4d4d4",
    UncommonBlue: "#6e89db",
    RarePurple: "#a664d1",
    EpicPink: "#b84d79",
    EnemyRed: "#b83a3a",
    LegendaryOrange: "#bfa545",
};
