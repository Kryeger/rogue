import Client from "./class/Client";
import App from "./components/App/App";
import ReactDOM from "react-dom";
import React from "react";

ReactDOM.render(<App />, document.getElementById("root"));
