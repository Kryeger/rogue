import { observer } from "mobx-react";
import React from "react";
import { useStores } from "../../stores/RootStore";
import ControlBar from "../ControlBar/ControlBar";
import Gold, { GoldType } from "../Gold/Gold";
import ExpBar from "./ExpBar/ExpBar";
import "./PlayerCard.scss";

const PlayerCard = () => {
    const { clientStore } = useStores();

    const player = clientStore?.user?.player;

    if (!player) return null;

    const percent = Math.max((player.hp / player.maxHp) * 100, 0);

    return (
        <div className={"player-card-wrap"}>
            <div className={"player-card-left"}>
                <div className={"player-card-exp-name"}>
                    <ExpBar />
                    <div className={"player-name-gold"}>
                        <div className={"player-card-name"}>{player.name}</div>
                        <div className={"player-card-gold"}>
                            <Gold amount={player.gold} />
                        </div>
                    </div>
                </div>

                <div className={"hp-bar"}>
                    <div className={"hp-bar-fill"} style={{ width: `${percent}%` }} />
                    <div className={"hp-bar-fill-slow"} style={{ width: `${percent}%` }} />
                </div>
            </div>
        </div>
    );
};

export default observer(PlayerCard);
