import { observer } from "mobx-react";
import React from "react";
import "./ExpBar.scss";
import { useStores } from "../../../stores/RootStore";

const ExpBar = () => {
    const { clientStore } = useStores();

    if (!clientStore.user?.player) return null;

    const { exp, toNextLevel, level } = clientStore.user?.player;

    const percent = (exp / toNextLevel) * 151;

    return (
        <div className={"exp-bar-wrap"}>
            <svg
                className="radial-bar"
                width={48}
                height={1}
                viewBox="0 0 52 52"
                xmlns="http://www.w3.org/2000/svg"
            >
                <circle className="radial-bar-background" cx="26" cy="26" r="24" />
                <circle
                    className="radial-bar-inside"
                    cx="26"
                    cy="26"
                    r="24"
                    style={{ strokeDasharray: `${percent} 151` }}
                />
            </svg>
            <div className={"exp-bar-center"}>{level}</div>
            <div className={"exp-bar-percent"}>{((percent / 151) * 100).toFixed(1)}%</div>
        </div>
    );
};

export default observer(ExpBar);
