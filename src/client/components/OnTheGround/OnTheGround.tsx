import React from "react";
import "./OnTheGround.scss";
import { observer } from "mobx-react";
import { useStores } from "../../stores/RootStore";
import InventoryItem, { ITEM_DISPLAY_TYPE } from "../Inventory/InventoryItem/InventoryItem";

const OnTheGround = () => {
    const {
        gameStore: { itemsOnTheGround },
    } = useStores();

    return (
        <div className={"on-the-ground-wrap"}>
            {itemsOnTheGround?.map((item, index) => (
                <InventoryItem key={index} item={item} displayType={ITEM_DISPLAY_TYPE.GROUND} />
            ))}
        </div>
    );
};

export default observer(OnTheGround);
