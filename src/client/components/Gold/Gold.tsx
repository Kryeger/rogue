import React from "react";
import "./Gold.scss";

export enum GoldType {
    numberWithIcon,
    number,
    circle,
}

const Gold = ({ amount, type }: { amount?: number; type?: GoldType }) => {
    if (!amount) return null;

    if (type === GoldType.numberWithIcon || !type) {
        return <div className={"gold"}>{`${amount}ⓖ`}</div>;
    }

    if (type === GoldType.circle) {
        return <div className={"gold-circle"}>{amount}</div>;
    }

    return null;
};

export default Gold;
