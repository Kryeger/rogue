import classNames from "classnames";
import { observer } from "mobx-react";
import React, { useState } from "react";
import "./ControlBar.scss";
import { useStores } from "../../stores/RootStore";
import Equipment from "../Equipment/Equipment";
import Inventory from "../Inventory/Inventory";

export enum ControlBarSelection {
    Inventory = "controlBarSelectionInventory",
    Equipment = "controlBarSelectionEquipment",
    Stats = "controlBarSelectionStats",
}

const ControlBar = () => {
    const {
        uiStore: { controlBarSelection, isLeaderboardOpen, setIsLeaderboardOpen },
        uiStore,
    } = useStores();

    const setSelection = (value: ControlBarSelection) => uiStore.setControlBarSelection(value);

    return (
        <div className={"control-bar-section-wrap"}>
            <div className={"control-bar-wrap"}>
                <div
                    className={classNames("control-bar-item", {
                        "selected": controlBarSelection === ControlBarSelection.Equipment,
                    })}
                    onClick={() => setSelection(ControlBarSelection.Equipment)}
                >
                    EQP
                </div>
                <div
                    className={classNames("control-bar-item", {
                        "selected": controlBarSelection === ControlBarSelection.Inventory,
                    })}
                    onClick={() => setSelection(ControlBarSelection.Inventory)}
                >
                    INV
                </div>
                <div
                    className={classNames("control-bar-item", {
                        "selected": controlBarSelection === ControlBarSelection.Stats,
                    })}
                    onClick={() => setSelection(ControlBarSelection.Stats)}
                >
                    STS
                </div>
                <div
                    className={classNames("control-bar-item", {
                        "selected": isLeaderboardOpen,
                    })}
                    onClick={() => setIsLeaderboardOpen(true)}
                >
                    LDB
                </div>
            </div>
        </div>
    );
};

export default observer(ControlBar);
