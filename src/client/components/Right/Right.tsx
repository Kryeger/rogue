import { observer } from "mobx-react";
import React from "react";
import "./Right.scss";
import EventLog from "../EventLog/EventLog";
import Settings from "../Settings/Settings";

const Right = () => {
    return (
        <div className={"right-wrap"}>
            <EventLog />
            <Settings />
        </div>
    );
};

export default observer(Right);
