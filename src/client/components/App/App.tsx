import React, { useRef } from "react";
import { useStores } from "../../stores/RootStore";
import { observer, Provider } from "mobx-react";
import { Route, Router } from "react-router-dom";
import history from "../../other/history";
import Main from "../Main/Main";

import "./App.scss";

const App = () => {
    return (
        <Provider value={useStores()}>
            <Router history={history}>
                <Route path={"/"}>
                    <Main />
                </Route>
            </Router>
        </Provider>
    );
};

export default observer(App);
