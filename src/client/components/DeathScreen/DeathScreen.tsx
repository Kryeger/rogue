import { observer } from "mobx-react";
import React from "react";
import "./DeathScreen.scss";
import { SMsg } from "../../../shared/interfaces/SMsg";
import { useStores } from "../../stores/RootStore";

const DeathScreen = () => {
    const { socketStore } = useStores();

    const onClick = () => socketStore.emit(SMsg.RESPAWN_REQ, {});

    return (
        <div className={"death-screen-wrap"}>
            <div className={"death-screen-text"}>You died. Respawn to try again.</div>
            <div className={"death-screen-respawn"} onClick={onClick}>
                Respawn
            </div>
        </div>
    );
};

export default observer(DeathScreen);
