import React from "react";
import { observer } from "mobx-react";

import "./Inventory.scss";
import InventoryItemList from "./InventoryItemList/InventoryItemList";
import { useStores } from "../../stores/RootStore";

const Inventory = () => {
    const {
        gameStore: { inventory },
    } = useStores();

    return (
        <div className={"inv-wrap"}>
            <InventoryItemList inventory={inventory} />
        </div>
    );
};

export default observer(Inventory);
