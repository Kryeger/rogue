import React from "react";
import { observer } from "mobx-react";
import { useStores } from "../../../stores/RootStore";
import InventoryItem from "../InventoryItem/InventoryItem";
import "./InventoryItemList.scss";
import { ItemInfo } from "../../../../server/class/Item";
import { Maybe } from "../../../../shared/interfaces/Maybe";

interface InventoryItemListProps {
    inventory?: Array<ItemInfo>;
}

const InventoryItemList = ({ inventory }: InventoryItemListProps) => {
    return (
        <div className={"inv-list"}>
            {/* For each item, create a new InventoryItem component with the item as its prop. */}
            {inventory?.map((item, index) => item && <InventoryItem key={index} item={item} />)}
        </div>
    );
};

export default observer(InventoryItemList);
