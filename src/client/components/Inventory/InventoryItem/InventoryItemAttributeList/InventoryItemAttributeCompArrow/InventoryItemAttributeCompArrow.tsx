import React from "react";
import { observer } from "mobx-react";
import "./InventoryItemAttributeCompArrow.scss";
import classNames from "classnames";

export enum ArrowType {
    UP,
    DOWN,
}

const InventoryItemAttributeCompArrow = ({ arrowType }: { arrowType: ArrowType }) => {
    return (
        <div
            className={classNames("inv-item-comp-arrow", {
                "up": arrowType === ArrowType.UP,
                "down": arrowType === ArrowType.DOWN,
            })}
        />
    );
};

export default observer(InventoryItemAttributeCompArrow);
