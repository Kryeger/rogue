import { observer } from "mobx-react";
import React from "react";
import { ItemInfo } from "../../../../../server/class/Item";
import {
    ItemAttribute,
    ItemAttributeInstance,
    ItemAttributes,
} from "../../../../../shared/interfaces/ItemAttributes";
import { ItemTypeToEquipmentSlot } from "../../../../../shared/interfaces/ItemTypes";
import { useStores } from "../../../../stores/RootStore";
import InventoryItemAttributeCompArrow, {
    ArrowType,
} from "./InventoryItemAttributeCompArrow/InventoryItemAttributeCompArrow";
import "./InventoryItemAttributeList.scss";

const InventoryItemAttributeList = ({
    item,
    dontCompare,
}: {
    item: ItemInfo;
    dontCompare?: boolean;
}) => {
    const {
        uiStore: { hoveredInvItem },
        gameStore: { player },
    } = useStores();

    const itemIsHovered =
        player && hoveredInvItem && player?.equipment && hoveredInvItem.uuid === item.uuid;

    const attributeIsBetter = (attr: ItemAttributeInstance) => {
        if (attr.attribute === ItemAttribute.Heal) return;

        const equipment = player?.equipment!;

        const equipmentSlot = ItemTypeToEquipmentSlot[
            hoveredInvItem?.itemType!
        ] as keyof typeof equipment;

        const equippedItem = player!.equipment[equipmentSlot];

        if (!equippedItem) return true;

        const matchingAttr =
            player!.equipment &&
            equippedItem.attributes.find(eqAttr => eqAttr.attribute === attr.attribute);

        if (!matchingAttr) return undefined;

        return attr.value > matchingAttr.value;
    };

    return (
        <div className={"inv-item-attrs"}>
            {item?.attributes?.map((attr, index) => (
                <div className={"inv-item-attr"} key={index}>
                    <div className={"inv-item-attr-name"}>
                        {ItemAttributes[attr?.attribute].name}
                    </div>
                    <div className={"inv-item-attr-value"}>
                        {attr.value.toFixed(2)}
                        {itemIsHovered &&
                            !dontCompare &&
                            (attributeIsBetter(attr) === true ? (
                                <InventoryItemAttributeCompArrow arrowType={ArrowType.UP} />
                            ) : attributeIsBetter(attr) === false ? (
                                <InventoryItemAttributeCompArrow arrowType={ArrowType.DOWN} />
                            ) : null)}
                    </div>
                </div>
            ))}
        </div>
    );
};

export default observer(InventoryItemAttributeList);
