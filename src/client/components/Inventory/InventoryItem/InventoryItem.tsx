import React from "react";

import "./InventoryItem.scss";
import { observer } from "mobx-react";
import { ItemInfo } from "../../../../server/class/Item";
import { useStores } from "../../../stores/RootStore";
import { SMsg } from "../../../../shared/interfaces/SMsg";
import classNames from "classnames";
import {
    faChevronDown,
    faChevronLeft,
    faChevronRight,
    faChevronUp,
} from "@fortawesome/free-solid-svg-icons";
import InventoryItemAttributeList from "./InventoryItemAttributeList/InventoryItemAttributeList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Gold from "../../Gold/Gold";

export enum ITEM_DISPLAY_TYPE {
    INVENTORY = "itemDisplayTypeInventory",
    EQUIPMENT = "itemDisplayTypeEquipment",
    GROUND = "itemDisplayTypeGround",
}

const InventoryItem = ({
    item,
    displayType,
}: {
    item?: ItemInfo;
    displayType?: ITEM_DISPLAY_TYPE;
}) => {
    const { socketStore, uiStore } = useStores();

    const equip = () => {
        item && socketStore.emit(SMsg.EQUIP_ITEM_REQ, { itemUuid: item.uuid });
    };

    const unequip = () => {
        item && socketStore.emit(SMsg.UNEQUIP_ITEM_REQ, { itemUuid: item.uuid });
    };

    const drop = () => {
        item && socketStore.emit(SMsg.DROP_ITEM_REQ, { itemUuid: item.uuid });
    };

    const pick = () => {
        item && socketStore.emit(SMsg.PICK_ITEM_REQ, { itemUuid: item.uuid });
    };

    const onMouseEnter = () => {
        uiStore.setHoveredInvItem(item);
    };

    const onMouseLeave = () => {
        uiStore.setHoveredInvItem(undefined);
    };

    const rarityClassName = item?.rarity.name.toLowerCase();

    const showComparison = displayType === ITEM_DISPLAY_TYPE.INVENTORY || !displayType;

    const isOnTheGround = displayType === ITEM_DISPLAY_TYPE.GROUND;

    const actions = {
        //If the item is shown in the inventory context, append the "equip" and "drop" actions,
        //each with its own css class, icon and function.
        [ITEM_DISPLAY_TYPE.INVENTORY]: [
            { className: "equip", icon: faChevronRight, fn: equip },
            { className: "drop", icon: faChevronDown, fn: drop },
        ],
        //Same here, in case of equipment.
        [ITEM_DISPLAY_TYPE.EQUIPMENT]: [
            { className: "unnequip", icon: faChevronLeft, fn: unequip },
        ],
        //Same here in case of items on the ground.
        [ITEM_DISPLAY_TYPE.GROUND]: [{ className: "pick", icon: faChevronUp, fn: pick }],
    }[displayType || ITEM_DISPLAY_TYPE.INVENTORY];
    //^Select the array of actions by the displayType prop (or a default value).

    return !item ? null : (
        <div
            className={classNames("inv-item-wrap", { "third": isOnTheGround })}
            style={{ borderColor: item.rarity.color }}
            {...{ onMouseEnter, onMouseLeave }}
        >
            <div className={"inv-item-info"}>
                <div className={classNames("inv-item-name")} style={{ color: item.rarity.color }}>
                    {item.name}
                    <Gold amount={item.price} />
                </div>
                <InventoryItemAttributeList item={item} dontCompare={!showComparison} />
            </div>
            <div className={"inv-item-actions"}>
                {actions.map((action, index) => (
                    <div
                        key={index}
                        className={classNames("inv-item-action", action.className, rarityClassName)}
                        onClick={action.fn}
                    >
                        <FontAwesomeIcon icon={action.icon} />
                    </div>
                ))}
            </div>
        </div>
    );
};

export default observer(InventoryItem);
