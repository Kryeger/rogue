import { observer } from "mobx-react";
import React, { useEffect, useRef } from "react";
import "./EventLog.scss";
import { useStores } from "../../stores/RootStore";
import EventLogItem from "./EventLogItem/EventLogItem";

const EventLog = () => {
    const {
        uiStore: { eventLogMessages },
    } = useStores();

    return (
        <div className={"event-log-wrap"}>
            <div className={"event-log-title"}>Event Log</div>
            <div className={"event-log-list"}>
                {eventLogMessages?.map((message, index) => (
                    <EventLogItem key={index} message={message} />
                ))}
            </div>
        </div>
    );
};

export default observer(EventLog);
