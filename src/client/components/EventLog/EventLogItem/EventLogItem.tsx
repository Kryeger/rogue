import { observer } from "mobx-react";
import React from "react";
import "./EventLogItem.scss";
import { EventLogMessage } from "../../../../shared/interfaces/eventLogTypes";

const EventLogItem = ({ message }: { message: EventLogMessage }) => {
    const result = message.message.map((el, index) => {
        if (el.color) {
            return (
                <span key={index} style={{ color: el.color }}>
                    {el.text}
                </span>
            );
        }
        return <span key={index}>{el.text}</span>;
    });

    return <div className={"event-log-item"}>{result}</div>;
};

export default observer(EventLogItem);
