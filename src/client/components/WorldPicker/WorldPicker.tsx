import { observer } from "mobx-react";
import React from "react";
import { SMsg } from "../../../shared/interfaces/SMsg";
import { WorldDifficulties } from "../../../shared/interfaces/WorldDifficulty";
import { useStores } from "../../stores/RootStore";

import "./WorldPicker.scss";

const WorldPicker = () => {
    const { socketStore, gameStore, clientStore } = useStores();

    const { user } = clientStore;

    const pickWorld = (worldUuid: string) => {
        socketStore.emit(SMsg.JOIN_WORLD_REQ, { worldUuid });
    };

    return (
        <div className={"world-picker-wrap"}>
            {user?.player?.name ? (
                <span className={"world-picker-text"}>
                    Hello, {user?.player?.name}. Pick a world to begin.
                </span>
            ) : null}
            {gameStore?.worlds?.map((world, index) => (
                <div key={index} className={"world-choice"} onClick={() => pickWorld(world?.uuid)}>
                    <span
                        style={{
                            backgroundColor: WorldDifficulties[world.difficulty].color,
                            color: "black",
                        }}
                        className={"world-choice-difficulty"}
                    >
                        {WorldDifficulties[world.difficulty].label}
                    </span>
                    &nbsp;
                    {`${world?.name} - ${world.playerCount} player${
                        world.playerCount !== 1 ? "s" : ""
                    }`}
                </div>
            ))}
        </div>
    );
};

export default observer(WorldPicker);
