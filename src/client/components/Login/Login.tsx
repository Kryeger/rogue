import classNames from "classnames";
import React, { useState } from "react";
import { observer } from "mobx-react";

import "./Login.scss";
import { useForm } from "react-hook-form";
import { useStores } from "../../stores/RootStore";
import { SMsg } from "../../../shared/interfaces/SMsg";

interface FormData {
    username: string;
    password: string;
    email?: string;
}

const Login = () => {
    const { register, handleSubmit } = useForm<FormData>();
    const [isRegister, setIsRegister] = useState(false);
    const { socketStore, uiStore } = useStores();

    const onSubmit = handleSubmit(({ username, password, email }) => {
        socketStore.emit(isRegister ? SMsg.REGISTER_REQ : SMsg.LOGIN_REQ, {
            username,
            password,
            email,
        });
        uiStore.setIsLoading(true);
    });

    const onPickerChoice = (choice: string) => {
        if (choice === "register") setIsRegister(true);
        else setIsRegister(false);
    };

    return (
        <div className={"login-wrap"}>
            <div className={"login-picker"}>
                <div
                    className={classNames("login-picker-btn", { "selected": !isRegister })}
                    onClick={() => onPickerChoice("login")}
                >
                    Login
                </div>
                <div
                    className={classNames("login-picker-btn", { "selected": isRegister })}
                    onClick={() => onPickerChoice("register")}
                >
                    Register
                </div>
            </div>
            {/* onSubmit function provided by the hook */}
            <form className={"login-form"} onSubmit={onSubmit}>
                {/* register function provided by the hook, returns a ref that is attached to the DOM element. */}
                {/* It also ensures validation rules which are passed as an argument. */}
                <input placeholder={"user"} name={"username"} ref={register({ required: true })} />
                <input
                    placeholder={"pass"}
                    name={"password"}
                    ref={register({ required: true })}
                    type="password"
                />
                <input type={"submit"} />
            </form>
        </div>
    );
};

export default observer(Login);
