import React from "react";
import { observer } from "mobx-react";

import "./CreatePlayer.scss";
import { useForm } from "react-hook-form";
import { useStores } from "../../stores/RootStore";
import { SMsg } from "../../../shared/interfaces/SMsg";

interface FormData {
    name: string;
}

const CreatePlayer = () => {
    const { register, handleSubmit } = useForm<FormData>();
    const { socketStore } = useStores();

    const onSubmit = handleSubmit(({ name }) => {
        socketStore.emit(SMsg.CREATE_PLAYER_REQ, { name });
    });

    return (
        <div className={"create-player-wrap"}>
            <span className={"create-text"}>Pick a name for your player.</span>
            <form className={"create-form"} onSubmit={onSubmit}>
                <input placeholder={"name"} name={"name"} ref={register({ required: true })} />
                <input type={"submit"} />
            </form>
        </div>
    );
};

export default observer(CreatePlayer);
