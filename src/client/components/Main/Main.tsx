import React from "react";
import DeathScreen from "../DeathScreen/DeathScreen";
import GameContainer from "../GameContainer/GameContainer";
import { observer } from "mobx-react";

import "./Main.scss";
import { useStores } from "../../stores/RootStore";
import Leaderboard from "../Leaderboard/Leaderboard";
import Login from "../Login/Login";
import classNames from "classnames";
import CreatePlayer from "../CreatePlayer/CreatePlayer";
import Loading from "../Loading/Loading";
import Right from "../Right/Right";
import WorldPicker from "../WorldPicker/WorldPicker";
import Left from "../Left/Left";

const Main = () => {
    const {
        clientStore: { isLoggedIn, hasPlayer, hasPickedWorld },
        gameStore: { playerIsDead },
        uiStore: { isLoading, isLeaderboardOpen },
    } = useStores();

    return (
        <>
            <div
                className={classNames("main-wrap", {
                    "blur":
                        !isLoggedIn ||
                        !hasPlayer ||
                        !hasPickedWorld ||
                        playerIsDead ||
                        isLeaderboardOpen,
                })}
            >
                <Left />
                <GameContainer />
                <Right />
            </div>

            {isLoading && <Loading />}
            {!isLoading && !isLoggedIn && <Login />}
            {!isLoading && isLoggedIn && !hasPlayer && <CreatePlayer />}

            {!isLoading && isLoggedIn && hasPlayer && !hasPickedWorld && !playerIsDead && (
                <WorldPicker />
            )}
            {!isLoading && isLoggedIn && hasPlayer && playerIsDead && <DeathScreen />}
            {!isLoading && isLeaderboardOpen && <Leaderboard />}
        </>
    );
};

export default observer(Main);
