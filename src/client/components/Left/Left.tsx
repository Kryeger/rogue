import React from "react";
import "./Left.scss";
import { observer } from "mobx-react";
import ControlBarContent from "../ControlBarContent/ControlBarContent";
import PlayerCard from "../PlayerCard/PlayerCard";

const Left = () => {
    return (
        <div className={"left-wrap"}>
            <PlayerCard />
            <ControlBarContent />
        </div>
    );
};

export default observer(Left);
