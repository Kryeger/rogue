import React, { useEffect, useRef } from "react";
import { useStores } from "../../stores/RootStore";
import { observer } from "mobx-react";
import "./GameContainer.scss";
import OnTheGround from "../OnTheGround/OnTheGround";

const GameContainer = () => {
    const { gameStore } = useStores();

    //Create a new ref.
    const gameRef = useRef<HTMLDivElement>(null);

    //Anytime the gameRef changes (which it will do once the render is done
    //and the ref has a DOM node linked to it), set the gameContainer to be
    //the ref's current HTML element.
    useEffect(() => {
        if (gameRef.current) {
            gameStore.setGameContainer(gameRef.current);
        }
    }, [gameRef]);

    const playerCount = gameStore.characters?.length;

    return (
        <div className={"game-wrap"}>
            <div className={"game-world-name"}>
                {gameStore.worldLabel?.name} {` - Floor ${gameStore.floorCount}`}
                {playerCount && ` - ${playerCount} player${playerCount === 1 ? "" : "s"}`}
            </div>
            <div className={"display-wrap"} ref={gameRef} />
            <div className={"game-bottom-wrap"}>
                <OnTheGround />
            </div>
        </div>
    );
};

export default observer(GameContainer);
