import { observer } from "mobx-react";
import React from "react";
import { SMsg } from "../../../shared/interfaces/SMsg";
import { useStores } from "../../stores/RootStore";
import "./Settings.scss";

const Settings = () => {
    const { socketStore } = useStores();

    const logout = () => {
        socketStore.emit(SMsg.LOGOUT_REQ, {});
    };

    return (
        <div className={"settings-wrap"}>
            <div className={"logout-button"} onClick={logout}>
                LOGOUT
            </div>
        </div>
    );
};

export default observer(Settings);
