import { observer } from "mobx-react";
import React from "react";
import { useStores } from "../../stores/RootStore";
import ControlBar, { ControlBarSelection } from "../ControlBar/ControlBar";
import Equipment from "../Equipment/Equipment";
import Inventory from "../Inventory/Inventory";
import Stats from "../Stats/Stats";

const ControlBarContent = () => {
    const {
        uiStore: { controlBarSelection },
    } = useStores();

    const items = {
        [ControlBarSelection.Inventory]: <Inventory />,
        [ControlBarSelection.Equipment]: <Equipment />,
        [ControlBarSelection.Stats]: <Stats />,
    };

    return (
        <div className={"cbc-wrap"}>
            <ControlBar />
            {controlBarSelection && items[controlBarSelection]}
        </div>
    );
};

export default observer(ControlBarContent);
