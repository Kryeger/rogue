import React from "react";

import "./Equipment.scss";
import { observer } from "mobx-react";
import { useStores } from "../../stores/RootStore";
import InventoryItem, { ITEM_DISPLAY_TYPE } from "../Inventory/InventoryItem/InventoryItem";

const Equipment = () => {
    const {
        gameStore: { player },
    } = useStores();

    if (!player) return null;

    const { equipment } = player;

    if (!equipment) return null;

    const { weapon, helmet, chestplate, pants, boots } = equipment;

    return (
        <div className={"equipment-wrap"}>
            <div className={"equipment-slots"}>
                {weapon && (
                    <div className={"equipment-slot"}>
                        <div className={"equipment-slot-title"}>Weapon</div>
                        <InventoryItem item={weapon} displayType={ITEM_DISPLAY_TYPE.EQUIPMENT} />
                    </div>
                )}
                {helmet && (
                    <div className={"equipment-slot"}>
                        <div className={"equipment-slot-title"}>Helmet</div>
                        <InventoryItem item={helmet} displayType={ITEM_DISPLAY_TYPE.EQUIPMENT} />
                    </div>
                )}
                {chestplate && (
                    <div className={"equipment-slot"}>
                        <div className={"equipment-slot-title"}>Chestplate</div>
                        <InventoryItem
                            item={chestplate}
                            displayType={ITEM_DISPLAY_TYPE.EQUIPMENT}
                        />
                    </div>
                )}
                {pants && (
                    <div className={"equipment-slot"}>
                        <div className={"equipment-slot-title"}>Pants</div>
                        <InventoryItem item={pants} displayType={ITEM_DISPLAY_TYPE.EQUIPMENT} />
                    </div>
                )}
                {boots && (
                    <div className={"equipment-slot"}>
                        <div className={"equipment-slot-title"}>Boots</div>
                        <InventoryItem item={boots} displayType={ITEM_DISPLAY_TYPE.EQUIPMENT} />
                    </div>
                )}
            </div>
        </div>
    );
};

export default observer(Equipment);
