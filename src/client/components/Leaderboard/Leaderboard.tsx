import classNames from "classnames";
import { observer } from "mobx-react";
import React, { useEffect, useRef, useState } from "react";
import { useEffectOnce } from "react-use";
import useOnClickOutside from "use-onclickoutside";
import { WorldDifficulties, WorldDifficulty } from "../../../shared/interfaces/WorldDifficulty";
import { useStores } from "../../stores/RootStore";
import "./Leaderboard.scss";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Leaderboard = () => {
    const [difficulty, setDifficulty] = useState(WorldDifficulty.Easy);
    const {
        socketStore,
        gameStore,
        gameStore: { fetchRecords, records },
        uiStore: { setIsLeaderboardOpen },
    } = useStores();

    const wrapRef = useRef<HTMLDivElement>(null);

    useEffectOnce(() => {
        fetchRecords();
    });

    const pickerItems = Object.values(WorldDifficulty).map((el, index) => {
        return (
            <div
                key={index}
                className={classNames("leaderboard-picker-item", { "selected": el === difficulty })}
                style={{
                    backgroundColor: WorldDifficulties[el].color,
                    color: "black",
                }}
                onClick={() => setDifficulty(el)}
            >
                {WorldDifficulties[el].label}
            </div>
        );
    });

    useOnClickOutside(wrapRef, () => setIsLeaderboardOpen(false));

    const processedRecords = records
        .slice()
        .sort((a, b) => b.maxFloor[difficulty] - a.maxFloor[difficulty])
        .map((record, index) =>
            record.maxFloor[difficulty] > 0 ? (
                <div key={index} className={"leaderboard-item"}>
                    <div
                        className={"leaderboard-item-text"}
                    >{`${record.userName} reached floor ${record.maxFloor[difficulty]} with ${record.playerName}`}</div>
                </div>
            ) : null
        );

    return (
        <div ref={wrapRef} className={"leaderboard-wrap"}>
            <div className={"leaderboard-close"} onClick={() => setIsLeaderboardOpen(false)}>
                <FontAwesomeIcon icon={faTimes} />
            </div>
            <div className={"leaderboard-title"}>Leaderboard</div>
            <div className={"leaderboard-picker"}>{pickerItems}</div>
            <div className={"leaderboard-list"}>
                {processedRecords.length ? (
                    processedRecords
                ) : (
                    <div className={"leaderboard-item gray"}>No records yet.</div>
                )}
            </div>
        </div>
    );
};

export default observer(Leaderboard);
