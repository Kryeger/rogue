import { observer } from "mobx-react";
import React from "react";
import "./Stats.scss";
import { ItemAttribute, ItemAttributeInstance } from "../../../shared/interfaces/ItemAttributes";
import { useStores } from "../../stores/RootStore";
import StatItem from "./StatItem/StatItem";

const Stats = () => {
    const {
        clientStore: { user },
    } = useStores();

    const equipment = user?.player?.equipment;

    if (!equipment) return null;

    //Create an object that will map each attribute to a pair of attribute and value.
    const attributes = {} as { [key in ItemAttribute]: ItemAttributeInstance };

    //For each equipped item
    Object.values(equipment).forEach(equippedItem => {
        //For each attribute
        equippedItem?.attributes.map(attr => {
            //Add the value of the attribute to the total.
            if (attributes[attr.attribute]) {
                attributes[attr.attribute].value += attr.value;
            } else {
                attributes[attr.attribute] = attr;
            }
        });
    });

    return (
        <div className={"stats-wrap"}>
            {attributes &&
                Object.values(attributes).map((attr, index) => (
                    //Pass each attribute obtained above to a specialized component
                    //That will style and display each one.
                    <StatItem key={index} attribute={attr} />
                ))}
        </div>
    );
};

export default observer(Stats);
