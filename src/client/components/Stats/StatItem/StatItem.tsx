import { observer } from "mobx-react";
import React from "react";
import "./StatItem.scss";
import {
    ItemAttributeInstance,
    ItemAttributes,
} from "../../../../shared/interfaces/ItemAttributes";

const StatItem = ({ attribute }: { attribute: ItemAttributeInstance }) => {
    return (
        <div className={"stat-item-wrap"}>
            <div
                className={"stat-item-circle"}
                style={{ borderColor: ItemAttributes[attribute.attribute].color }}
            >
                {attribute.value.toFixed(1)}
            </div>
            <div className={"stat-item-title"}>{ItemAttributes[attribute.attribute].acronym}</div>
        </div>
    );
};

export default observer(StatItem);
