import React from "react";
import { observer } from "mobx-react";

const Loading = () => {
    return <div className={"loading"} />;
};

export default observer(Loading);
