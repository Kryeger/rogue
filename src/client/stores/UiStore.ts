import { action, makeObservable, observable } from "mobx";
import { EnemyInfo } from "../../server/class/Enemy";
import { ItemInfo } from "../../server/class/Item";
import { EventLogMessage } from "../../shared/interfaces/eventLogTypes";
import { Maybe } from "../../shared/interfaces/Maybe";
import { ControlBarSelection } from "../components/ControlBar/ControlBar";
import { RootStore } from "./RootStore";

export default class UiStore {
    constructor(rootStore: RootStore) {
        makeObservable(this);

        this.rootStore = rootStore;
    }

    public rootStore: RootStore;
    @observable private _isLoading: boolean = false;

    get isLoading(): boolean {
        return this._isLoading;
    }

    @action setIsLoading(value: boolean) {
        this._isLoading = value;
    }

    @observable private _hoveredInvItem: Maybe<ItemInfo>;

    get hoveredInvItem(): Maybe<ItemInfo> {
        return this._hoveredInvItem;
    }

    @action setHoveredInvItem = (value: Maybe<ItemInfo>) => {
        this._hoveredInvItem = value;
    };

    @observable private _controlBarSelection: ControlBarSelection = ControlBarSelection.Equipment;

    get controlBarSelection(): ControlBarSelection {
        return this._controlBarSelection;
    }

    @action setControlBarSelection = (value: ControlBarSelection) => {
        this._controlBarSelection = value;
    };

    @observable private _eventLogMessages: Array<EventLogMessage> = [];

    get eventLogMessages(): Array<EventLogMessage> {
        return this._eventLogMessages;
    }

    @observable private _isLeaderboardOpen: boolean = false;

    get isLeaderboardOpen(): boolean {
        return this._isLeaderboardOpen;
    }

    @action setIsLeaderboardOpen = (value: boolean) => {
        this._isLeaderboardOpen = value;
    };

    @action removeEventLogMessage = (value: EventLogMessage) => {
        this._eventLogMessages = this._eventLogMessages.filter(
            el => el.timestamp !== value.timestamp
        );
    };

    @action pushEventLogMessage = (value: EventLogMessage) => {
        setTimeout(() => {
            this.removeEventLogMessage(value);
        }, 10000);
        this._eventLogMessages = [value, ...this._eventLogMessages];
    };
}
