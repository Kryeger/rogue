import ClientStore from "./ClientStore";
import { createContext, useContext } from "react";
import GameStore from "./GameStore";
import SocketStore from "./SocketStore";
import UiStore from "./UiStore";

export class RootStore {
    private readonly _clientStore: ClientStore;
    private readonly _gameStore: GameStore;
    private readonly _socketStore: SocketStore;
    private readonly _uiStore: UiStore;

    constructor() {
        this._socketStore = new SocketStore(this);
        this._clientStore = new ClientStore(this);
        this._gameStore = new GameStore(this);
        this._uiStore = new UiStore(this);
    }

    get uiStore(): UiStore {
        return this._uiStore;
    }

    get socketStore(): SocketStore {
        return this._socketStore;
    }

    get gameStore(): GameStore {
        return this._gameStore;
    }

    get clientStore(): ClientStore {
        return this._clientStore;
    }
}

//Instantiate a new RootStore.
const rootStore = new RootStore();

//Create a context using the RootStore structure.
export const RootStoreContext = createContext(rootStore);

//Hook used for providing access to the RootStore throughout the frontend.
export const useStores = () => {
    const store = useContext(RootStoreContext);
    if (!store) {
        throw new Error("RootStore is null.");
    }
    return store;
};
