import { Display } from "rot-js";
import { action, computed, makeObservable, observable, toJS } from "mobx";
import { WorldDifficulty } from "../../shared/interfaces/WorldDifficulty";
import { RootStore } from "./RootStore";
import { Maybe } from "../../shared/interfaces/Maybe";
import { SMsg } from "../../shared/interfaces/SMsg";
import { InputTypes } from "../../shared/interfaces/InputTypes";
import { Colors } from "../other/Colors";
import { Vector2 } from "../../shared/interfaces/positions";
import { FloorInfo } from "../../shared/class/Floor";
import {
    CharacterInfo,
    EnemyInfo,
    ItemInfo,
    Record,
    TileInfo,
    WorldLabel,
} from "../../server/server_internal";

export default class GameStore {
    public rootStore: RootStore;
    public display: Display;
    @observable public gameContainer: HTMLElement | null = null;
    @observable private _worldLabel: Maybe<WorldLabel>;
    @observable private _worlds: Maybe<Array<WorldLabel>>;
    @observable private _floorCount: Maybe<number>;
    @observable private _matrix: Maybe<Array<Array<Maybe<TileInfo>>>>;
    @observable private _items: Array<ItemInfo> = [];
    @observable private _characters: Array<CharacterInfo> = [];
    @observable private _enemies: Array<EnemyInfo> = [];
    @observable private _spawnPosition: Maybe<Vector2>;
    @observable private _leavePosition: Maybe<Vector2>;
    @observable private _records: Array<Record> = [];

    constructor(rootStore: RootStore) {
        makeObservable(this);
        this.rootStore = rootStore;
        this.display = new Display({
            layout: "rect",
            width: 70,
            height: 30,
            bg: "black",
            fg: "white",
            fontSize: 20,
        });

        this.rootStore.socketStore.emit(SMsg.GET_WORLDS_REQ, {});

        this.initControls();
    }

    get records(): Array<Record> {
        return this._records;
    }

    get spawnPosition(): Maybe<Vector2> {
        return this._spawnPosition;
    }

    get leavePosition(): Maybe<Vector2> {
        return this._leavePosition;
    }

    get worldLabel(): Maybe<WorldLabel> {
        return this._worldLabel;
    }

    @computed get player() {
        return this.rootStore.clientStore.user?.player;
    }

    @computed get inventory() {
        return this.player?.inventory?.filter(
            item => !Object.values(this.player?.equipment || {})?.find(el => el?.uuid === item.uuid)
        );
    }

    @computed get itemsOnTheGround() {
        return this.getItemsByPosition(
            this.characters?.find(char => char.uuid === this.player?.charUuid)?.position
        );
    }

    @computed get playerIsDead() {
        return this.player?.hp && this.player.hp <= 0;
    }

    get characters(): Array<CharacterInfo> {
        return this._characters;
    }

    get worlds(): Maybe<Array<WorldLabel>> {
        return this._worlds;
    }

    get items(): Array<Maybe<ItemInfo>> {
        return this._items;
    }

    get floorCount(): Maybe<number> {
        return this._floorCount;
    }

    get matrix(): Maybe<Array<Array<Maybe<TileInfo>>>> {
        return this._matrix;
    }

    get enemies(): Array<EnemyInfo> {
        return this._enemies;
    }

    @action clearGame = () => {
        delete this._leavePosition;
        delete this._spawnPosition;
        delete this._floorCount;
        delete this._worldLabel;
        delete this._matrix;
        this._characters = [];
        this._enemies = [];
        this._worlds = [];
        this._items = [];
        this._records = [];
    };

    @action setRecords = (value: Maybe<Array<Record>>) => {
        value && (this._records = value);
    };

    @action setWorlds = (value: Maybe<Array<WorldLabel>>) => {
        value && (this._worlds = value);
    };

    @action setItems = (value: Array<ItemInfo>) => {
        value && (this._items = value);
    };

    @action setFloor = (value: Maybe<FloorInfo>) => {
        if (!value) return;
        value.matrix && (this._matrix = value.matrix);
        this.setCharacters(value.characters);
        this.setItems(value.items);
        this.setFloorCount(value.floorCount);
        this.setSpawnAndLeavePositon(value.spawnPosition, value.leavePosition);
        this.setEnemies(value.enemies);
    };

    @action setSpawnAndLeavePositon = (spawnPosition: Vector2, leavePosition: Vector2) => {
        this._spawnPosition = spawnPosition;
        this._leavePosition = leavePosition;
        this.draw();
    };

    @action setEnemies = (value: Array<EnemyInfo>) => {
        value && (this._enemies = value);
    };

    @action setCharacters = (characters: Array<CharacterInfo>) => {
        characters && (this._characters = characters);
    };

    @action setGameContainer = (container: HTMLElement) => {
        this.gameContainer = container;
        this.gameContainer?.appendChild(this.display.getContainer()!);
    };

    @action setWorldLabel = (value: Maybe<WorldLabel>) => {
        value && (this._worldLabel = value);
    };

    @action setFloorCount = (value: number) => {
        value !== undefined && (this._floorCount = value);
    };

    @action fetchRecords = () => {
        this.rootStore.socketStore.emit(SMsg.GET_RECORDS_REQ, {});
    };

    draw = () => {
        this.matrix?.forEach((row, rowIndex) =>
            row.forEach((el, colIndex) => {
                el?.tileTypeInfo?.char &&
                    this.display.draw(
                        rowIndex,
                        colIndex,
                        el?.tileTypeInfo?.char,
                        Colors.InertGray,
                        Colors.Black
                    );
            })
        );

        this.items?.forEach(item => {
            if (!item || !item?.position) return;

            this.display.draw(
                item.position.x,
                item.position.y,
                ".",
                Colors.InertGray,
                Colors.Black
            );
        });

        this.spawnPosition &&
            this.floorCount !== 0 &&
            this.display.draw(
                this.spawnPosition.x,
                this.spawnPosition.y,
                "˅",
                Colors.White,
                Colors.Black
            );

        this.leavePosition &&
            this.display.draw(
                this.leavePosition.x,
                this.leavePosition.y,
                "˄",
                Colors.White,
                Colors.Black
            );

        this.enemies?.forEach(enemy => {
            this.display.draw(
                enemy.position.x,
                enemy.position.y,
                enemy.targetCharUuid === this.player?.charUuid ? "ĕ" : "e",
                Colors.EnemyRed,
                Colors.Black
            );
        });

        this.characters?.forEach(char => {
            this.display.draw(
                char.position.x,
                char.position.y,
                "@",
                Colors.PlayerGreen,
                Colors.Black
            );
        });
    };

    getItemsByPosition = (position: Maybe<Vector2>) => {
        return this.items?.filter(
            item => item?.position?.x === position?.x && item?.position?.y === position?.y
        );
    };

    initControls = () => {
        window.addEventListener("keypress", e => {
            if (e.code === "KeyW") {
                this.rootStore.socketStore.emit(SMsg.INPUT_REQ, { input: InputTypes.INPUT_UP });
            }
            if (e.code === "KeyD") {
                this.rootStore.socketStore.emit(SMsg.INPUT_REQ, { input: InputTypes.INPUT_RIGHT });
            }
            if (e.code === "KeyS") {
                this.rootStore.socketStore.emit(SMsg.INPUT_REQ, { input: InputTypes.INPUT_DOWN });
            }
            if (e.code === "KeyA") {
                this.rootStore.socketStore.emit(SMsg.INPUT_REQ, { input: InputTypes.INPUT_LEFT });
            }
            if (e.code === "KeyE") {
                //Should exist.
                const character = this.characters.find(el => el.uuid === this.player?.charUuid)!;

                if (
                    character.position.x === this.spawnPosition?.x &&
                    character.position.y === this.spawnPosition?.y &&
                    this.floorCount !== 0
                ) {
                    this.rootStore.socketStore.emit(SMsg.CHANGE_FLOOR_REQ, {});
                    return;
                }
                if (
                    character.position.x === this.leavePosition?.x &&
                    character.position.y === this.leavePosition?.y
                ) {
                    this.rootStore.socketStore.emit(SMsg.CHANGE_FLOOR_REQ, {});
                    return;
                }

                this.rootStore.socketStore.emit(SMsg.INPUT_REQ, { input: InputTypes.INPUT_ACTION });
            }
        });
    };
}
