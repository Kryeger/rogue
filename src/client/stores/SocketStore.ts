import { makeObservable, observable, toJS } from "mobx";
import socketio from "socket.io-client";
import { Auth } from "../../shared/interfaces/Auth";
import { EventLogMessage } from "../../shared/interfaces/eventLogTypes";
import { SMsg } from "../../shared/interfaces/SMsg";
import { RootStore } from "./RootStore";

export default class SocketStore {
    public rootStore: RootStore;

    @observable socket: SocketIOClient.Socket;

    constructor(rootStore: RootStore) {
        makeObservable(this);
        console.log(`${process.env.IP}:${process.env.PORT}`);
        this.rootStore = rootStore;
        this.socket = socketio(`${process.env.IP}:${process.env.PORT}`);

        this.socket
            .on("connect", () => {
                console.log("Connected through socket:", this.socket.id);

                this.emit(SMsg.CONNECT_REQ, {});
                this.rootStore.uiStore.setIsLoading(true);
            })
            //When a CONNECT_RES message is detected,
            .on(SMsg.CONNECT_RES, (data: any) => {
                //Hide the loading screen.
                this.rootStore.uiStore.setIsLoading(false);

                //If the request failed, delete the Auth object stored in localStorage.
                if (!data?.success) {
                    this.clearAuth();
                    console.log(data?.message);
                    return;
                }

                //Set the user field in the ClientStore
                //as the user object that the server sent.
                this.rootStore.clientStore.setUser(data?.user);

                const worldUuid = data?.user?.player?.worldUuid;

                //If the user's players has a worldUuid
                if (worldUuid) {
                    //Emit the request to get the world's details.
                    this.emit(SMsg.GET_WORLD_REQ, { worldUuid });
                }
            })
            .on(SMsg.REGISTER_RES, (data: any) => {
                this.rootStore.uiStore.setIsLoading(false);
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }

                this.setAuth(data?.auth);
                this.rootStore.clientStore.setUser(data?.user);
            })
            .on(SMsg.LOGIN_RES, (data: any) => {
                this.rootStore.uiStore.setIsLoading(false);
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }
                this.setAuth(data?.auth);
                this.rootStore.clientStore.setUser(data?.user);

                this.emit(SMsg.GET_WORLDS_REQ, {});
            })
            .on(SMsg.CREATE_PLAYER_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }
                this.rootStore.clientStore.setUser(data?.user);
            })
            .on(SMsg.GET_WORLDS_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }
                this.rootStore.gameStore.setWorlds(data?.worlds);
            })
            .on(SMsg.JOIN_WORLD_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }
                this.rootStore.clientStore.setUser(data?.user);
                this.rootStore.gameStore.setFloor(data?.floor);
                this.rootStore.gameStore.setWorldLabel(data?.worldLabel);

                this.rootStore.gameStore.draw();

                data?.messages?.map(
                    (el: EventLogMessage) => el && this.rootStore.uiStore.pushEventLogMessage(el)
                );
            })
            //Whenever a MAP_UPDATE_RES response is received
            .on(SMsg.MAP_UPDATE_RES, (data: any) => {
                //If the success field is false, nothing is done.
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }

                //Use the information received to update the game state.
                this.rootStore.clientStore.setUser(data?.user); //type is User
                this.rootStore.gameStore.setCharacters(data?.characters); //type is Array<CharacterInfo>
                this.rootStore.gameStore.setItems(data?.items); //type is Array<ItemInfo>
                this.rootStore.gameStore.setEnemies(data?.enemies); //type is Array<EnemyInfo>

                this.rootStore.gameStore.setFloorCount(data?.floorCount);

                this.rootStore.gameStore.draw();

                data?.messages?.map(
                    (el: EventLogMessage) => el && this.rootStore.uiStore.pushEventLogMessage(el)
                );
            })
            .on(SMsg.PICK_ITEM_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }

                this.rootStore.clientStore.setUser(data.user);
                this.rootStore.gameStore.setItems(data.items);

                this.rootStore.gameStore.draw();
            })
            .on(SMsg.EQUIP_ITEM_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }
                this.rootStore.clientStore.setUser(data.user);

                this.rootStore.uiStore.setHoveredInvItem(undefined);
            })
            .on(SMsg.DROP_ITEM_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }
                this.rootStore.clientStore.setUser(data.user);
                this.rootStore.gameStore.setItems(data.items);

                this.rootStore.gameStore.draw();
            })
            .on(SMsg.UNEQUIP_ITEM_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }
                this.rootStore.clientStore.setUser(data.user);

                this.rootStore.uiStore.setHoveredInvItem(undefined);
            })
            .on(SMsg.CHANGE_FLOOR_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }

                console.log("CHANGE FLOOR RES", { data });

                this.rootStore.clientStore.setUser(data.user);
                this.rootStore.gameStore.setFloor(data.floor);

                this.rootStore.gameStore.draw();
            })
            .on(SMsg.RESPAWN_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }

                this.rootStore.clientStore.setUser(data.user);
            })
            .on(SMsg.GET_RECORDS_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }

                this.rootStore.gameStore.setRecords(data.records);
            })
            .on(SMsg.LOGOUT_RES, (data: any) => {
                if (!data?.success) {
                    console.log(data?.message);
                    return;
                }

                this.clearAuth();
                this.rootStore.clientStore.clearUser();
                this.rootStore.gameStore.clearGame();
            });
    }
    setAuth = (auth: Auth) => {
        if (auth) {
            localStorage.setItem("auth", JSON.stringify(auth));
        }
    };
    clearAuth = () => localStorage.removeItem("auth");

    getAuth = () => JSON.parse(localStorage.getItem("auth") || "{}");

    emit = (msg: SMsg, data: any) => {
        //Get the auth object from localStorage.
        const auth = this.getAuth();

        //Append it to the object that is passed to the server,
        //along the other data and the message.
        this.socket.emit(msg, { ...data, auth });
    };
}
