import { User } from "../../server/models";
import { Maybe } from "../../shared/interfaces/Maybe";
import { RootStore } from "./RootStore";
import { action, computed, makeObservable, observable, toJS } from "mobx";

export default class ClientStore {
    rootStore: RootStore;

    constructor(rootStore: RootStore) {
        makeObservable(this);
        this.rootStore = rootStore;
    }

    @observable private _user: Maybe<Partial<User>>;

    get user(): Maybe<Partial<User>> {
        return this._user;
    }

    @action setUser(value: Maybe<Partial<User>>) {
        value && (this._user = value);
    }

    @action clearUser = () => {
        this._user = undefined;
    };

    @computed get isLoggedIn() {
        return this.user !== undefined;
    }

    @computed get hasPlayer() {
        return this.user?.player !== undefined;
    }

    @computed get hasPickedWorld() {
        return this.user?.player?.worldUuid !== undefined;
    }
}
