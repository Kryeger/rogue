import { Server, WorldManager } from "../server_internal";

export default async (data: any) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const worlds = Array.from(WorldManager.instances.values(), world => world.getLabelInfo());

    return { success: true, worlds };
};
