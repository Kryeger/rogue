import { SMsg } from "../../shared/interfaces/SMsg";
import {
    CharacterManager,
    PlayerManager,
    Server,
    UserManager,
    WorldManager,
} from "../server_internal";

export default async (data: any) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { uuid } = data.auth;
    const { itemUuid } = data;

    if (!itemUuid) {
        return {
            success: false,
            message: "Bad data",
        };
    }

    const playerDoc = (await (await UserManager.get(uuid))?.populateAll())?.player;

    if (!playerDoc) {
        return {
            success: false,
            message: "Player not found.",
        };
    }

    if (playerDoc.hp <= 0) {
        return {
            success: false,
            message: "Player is dead.",
        };
    }

    if (!playerDoc.worldUuid || !playerDoc.charUuid) {
        return {
            success: false,
            message: "Player missing data.",
        };
    }

    const character = CharacterManager.get(playerDoc.charUuid);

    if (!character) {
        return {
            success: false,
            message: "Character not found.",
        };
    }

    const success = await PlayerManager.drop(playerDoc.uuid, itemUuid);

    if (success) {
        const floor = WorldManager.get(playerDoc.worldUuid)!.gameMap.floors[character.position.z];

        Server.io.to(floor.uuid).emit(SMsg.MAP_UPDATE_RES, {
            success: true,
            items: floor.getResolvedItems().map(item => item.getInfo()),
        });

        return {
            success: true,
            user: await (await UserManager.get(uuid))?.getInfo(),
        };
    }
};
