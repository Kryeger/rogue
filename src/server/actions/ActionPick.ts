import { SMsg } from "../../shared/interfaces/SMsg";
import {
    CharacterManager,
    ItemDocManager,
    ItemManager,
    Server,
    UserManager,
    WorldManager,
} from "../server_internal";

export default async (data: any) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { uuid } = data.auth;
    const { itemUuid } = data;

    //TODO: Function that solves context (user, player, character, world etc).

    const user = await (await UserManager.get(uuid))?.getInfo();

    if (!user) {
        return {
            success: false,
            message: "User not found.",
        };
    }

    if (!user.player) {
        return {
            success: false,
            message: "Player not found.",
        };
    }

    if (user.player.hp <= 0) {
        return {
            success: false,
            message: "Player is dead.",
        };
    }

    if (!user.player.worldUuid || !user.player.charUuid) {
        return {
            success: false,
            message: "Player missing data.",
        };
    }

    const character = CharacterManager.get(user.player.charUuid);

    if (!character) {
        return {
            success: false,
            message: "Character not found.",
        };
    }

    const world = await WorldManager.get(user?.player?.worldUuid);

    if (!world) {
        return {
            success: false,
            message: "World not found.",
        };
    }

    const item = await ItemManager.get(itemUuid);

    if (!item) {
        return {
            success: false,
            message: "Item not found.",
        };
    }

    const result = await ItemDocManager.createAndInsert(item, user.player.uuid);

    if (result) {
        const floor = world.gameMap.floors[character.position.z];

        floor.removeItem(item.uuid);

        Server.io.to(floor.uuid).emit(SMsg.MAP_UPDATE_RES, {
            success: true,
            items: world.gameMap.floors[character.position.z]
                .getResolvedItems()
                .map(item => item.getInfo()),
        });

        return {
            success: true,
            user: await (await UserManager.get(user.uuid))?.getInfo(),
        };
    }
};
