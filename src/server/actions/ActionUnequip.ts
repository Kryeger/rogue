import { Server, UserManager } from "../server_internal";

export default async (data: any) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { uuid } = data.auth;
    const { itemUuid } = data;

    if (!itemUuid) {
        return {
            success: false,
            message: "Bad data",
        };
    }

    const userDoc = await UserManager.get(uuid);

    if (!userDoc) {
        return {
            success: false,
            message: "User not found.",
        };
    }

    const playerDoc = await userDoc.getPlayer();

    if (playerDoc.hp <= 0) {
        return {
            success: false,
            message: "Player is dead.",
        };
    }

    const success = await playerDoc.unequip(itemUuid);

    if (success) {
        return {
            success: true,
            user: await userDoc.getInfo(),
        };
    }

    return {
        success: false,
        message: "Failed to unequip.",
    };
};
