import { Socket } from "socket.io";
import { SMsg } from "../../shared/interfaces/SMsg";
import {
    CharacterManager,
    PlayerManager,
    Server,
    UserManager,
    WorldManager,
} from "../server_internal";

export default async (data: any, socket?: Socket) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { uuid } = data.auth;

    const userDoc = await UserManager.get(uuid);
    const playerDoc = await userDoc?.getPlayer();

    if (!userDoc) {
        return {
            success: false,
            message: "User not found.",
        };
    }

    if (!playerDoc) {
        return {
            success: false,
            message: "Player not found.",
        };
    }

    if (playerDoc.hp > 0) {
        return {
            success: false,
            message: "Player is not dead",
        };
    }

    if (!playerDoc.worldUuid || !playerDoc.charUuid) {
        return {
            success: false,
            message: "Player missing data.",
        };
    }

    const world = await WorldManager.get(playerDoc.worldUuid);

    if (!world) {
        return {
            success: false,
            message: "World not found.",
        };
    }

    const character = await CharacterManager.get(playerDoc.charUuid);

    if (!character) {
        return {
            success: false,
            message: "Character not found.",
        };
    }

    const floor = world.gameMap.floors[character.position.z];

    socket && socket.leave(floor.uuid);

    userDoc.player = undefined;
    await floor.removePlayer(playerDoc.uuid);
    await PlayerManager.delete(playerDoc.uuid);

    await userDoc.save();

    Server.io.to(floor.uuid).emit(SMsg.MAP_UPDATE_RES, {
        success: true,
        characters: floor.getResolvedCharacters().map(item => item.getInfo()),
    });

    return {
        success: true,
        user: await (await UserManager.get(uuid))?.getInfo(),
    };
};
