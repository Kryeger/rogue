import { Socket } from "socket.io";
import { Colors } from "../../client/other/Colors";
import { EventLogMessageType } from "../../shared/interfaces/eventLogTypes";
import { SMsg } from "../../shared/interfaces/SMsg";
import { Server, UserManager, WorldManager } from "../server_internal";

export default async (data: any, socket?: Socket) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { worldUuid } = data;

    if (!worldUuid) {
        return {
            success: false,
            message: "Bad data.",
        };
    }

    const world = WorldManager.get(worldUuid);

    if (!world) {
        return {
            success: false,
            message: "World not found.",
        };
    }

    const { uuid } = data.auth;

    let userDoc = await UserManager.get(uuid);
    const playerDoc = await userDoc?.getPlayer();

    if (!playerDoc) {
        return {
            success: false,
            message: "User doesn't have a player.",
        };
    }

    if (playerDoc.hp <= 0) {
        return {
            success: false,
            message: "Player is dead.",
        };
    }

    if (!(await WorldManager.insertPlayer(worldUuid, playerDoc.uuid))) {
        return {
            success: false,
            message: "Failed to insert player.",
        };
    }

    const floor = world.gameMap.floors[0];

    //Should exist.
    socket?.join(floor.uuid);

    Server.io.to(floor.uuid).emit(SMsg.MAP_UPDATE_RES, {
        success: true,
        characters: await floor.getResolvedCharacters().map(el => el.getInfo()),
    });

    const updatedUserDoc = await UserManager.get(uuid);

    return {
        success: true,
        user: await updatedUserDoc?.getInfo(),
        floor: await floor.getInfo(),
        worldLabel: world?.getLabelInfo(),
        messages: [
            {
                type: EventLogMessageType.Join,
                timestamp: Date.now(),
                message: [
                    { text: playerDoc.name, color: Colors.UncommonBlue },
                    { text: " joined the world." },
                ],
            },
        ],
    };
};
