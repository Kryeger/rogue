import { Socket } from "socket.io";
import { Server, UserManager } from "../server_internal";

export default async (data: any, socket: Socket) => {
    const { username, password, email } = data;

    if (!username || !password) {
        return {
            success: false,
            message: "Bad data.",
        };
    }

    const newUser = await UserManager.register(username, password);

    if (!newUser) {
        return { success: false, message: "Register failed." };
    }

    const auth = Server.setAuth(newUser?.uuid, socket);

    return {
        success: true,
        user: await newUser?.getInfo(),
        auth,
    };
};
