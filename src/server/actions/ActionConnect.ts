import { Socket } from "socket.io";
import { CharacterManager, Server, UserManager, WorldManager } from "../server_internal";

export default async (data: any, socket: Socket) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { uuid } = data.auth;
    const foundUser = await UserManager.get(uuid);
    const player = await foundUser?.getPlayer();

    if (player?.worldUuid && player?.charUuid) {
        const world = WorldManager.get(player.worldUuid);
        const char = CharacterManager.get(player.charUuid);

        if (world && char) {
            //Should exist.
            const floor = world!.gameMap.floors[char!.position.z];
            socket.join(floor.uuid);
        }
    }

    if (foundUser) {
        const timeout = Server.secrets.find(el => el.uuid === uuid)?.disconnectTimeout;
        timeout && clearTimeout(timeout);

        return {
            success: true,
            user: await foundUser?.getInfo(),
        };
    }

    return {
        success: false,
        message: "No user found for this secret.",
    };
};
