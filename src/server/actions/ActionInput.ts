import Chance from "chance";
import { Colors } from "../../client/other/Colors";
import { TargetType } from "../../shared/class/Floor";
import { EventLogMessage, EventLogMessageType } from "../../shared/interfaces/eventLogTypes";
import { InputTypes } from "../../shared/interfaces/InputTypes";
import { Maybe } from "../../shared/interfaces/Maybe";
import { Vector3 } from "../../shared/interfaces/positions";
import { SMsg } from "../../shared/interfaces/SMsg";
import Enemy from "../class/Enemy";
import RecordManager from "../managers/RecordManager";
import {
    CharacterManager,
    EnemyManager,
    ItemDocManager,
    PlayerDoc,
    Server,
    UserManager,
    WorldManager,
} from "../server_internal";

const chance = new Chance();

export default async (data: any) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { uuid } = data.auth;
    const { input }: { input: InputTypes } = data;

    const user = await (await UserManager.get(uuid))?.populate("player").execPopulate();

    if (!user) {
        return {
            success: false,
            message: "User not found.",
        };
    }

    const playerDoc = user.player as PlayerDoc;

    if (playerDoc.hp <= 0) {
        return {
            success: false,
            message: "Player is dead.",
        };
    }

    if (!playerDoc.worldUuid || !playerDoc.charUuid) {
        return {
            success: false,
            message: "Player missing data.",
        };
    }

    const world = await WorldManager.get(playerDoc.worldUuid);

    if (!world) {
        return {
            success: false,
            message: "World not found.",
        };
    }

    const character = await CharacterManager.get(playerDoc.charUuid);

    if (!character) {
        return {
            success: false,
            message: "Character not found.",
        };
    }

    const floor = world.gameMap.floors[character.position.z];

    const messages: Array<EventLogMessage> = [];

    let move: Vector3;
    let target: Maybe<Enemy>;

    //Handle movement.
    switch (input) {
        case InputTypes.INPUT_UP:
            move = {
                x: character.position.x,
                y: character.position.y - 1,
                z: character.position.z,
            };
            break;

        case InputTypes.INPUT_RIGHT:
            move = {
                x: character.position.x + 1,
                y: character.position.y,
                z: character.position.z,
            };
            break;

        case InputTypes.INPUT_DOWN:
            move = {
                x: character.position.x,
                y: character.position.y + 1,
                z: character.position.z,
            };
            break;

        case InputTypes.INPUT_LEFT:
            move = {
                x: character.position.x - 1,
                y: character.position.y,
                z: character.position.z,
            };
            break;
    }

    if (
        input === InputTypes.INPUT_DOWN ||
        input === InputTypes.INPUT_LEFT ||
        input === InputTypes.INPUT_UP ||
        input === InputTypes.INPUT_RIGHT
    )
        if (world.gameMap.checkMove(move!)) {
            character.position = move!;
        } else {
            target = world.gameMap.checkTarget(move!, TargetType.Enemy) as Maybe<Enemy>;

            if (target) {
                const [playerSpeed, playerDamage, playerDefense] = await Promise.all([
                    playerDoc.getSpeed(),
                    playerDoc.getDamage(),
                    playerDoc.getDefense(),
                ]);

                const blockedDamage = chance.floating({
                    min: playerDefense / 2,
                    max: playerDefense,
                });
                const potentialDamage = Math.max(target.damage - blockedDamage, 0);
                const potentialLeftHp = Math.max(target.hp - playerDamage, 0);

                const playerAttacksMessage = {
                    message: [
                        { text: playerDoc.name, color: Colors.UncommonBlue },
                        { text: " dealt " },
                        { text: playerDamage.toFixed(2), color: Colors.PlayerGreen },
                        { text: " damage. " },
                        { text: target.name, color: Colors.EnemyRed },
                        { text: " has " },
                        { text: potentialLeftHp.toFixed(2), color: Colors.PlayerGreen },
                        { text: " HP left." },
                    ],
                    timestamp: Date.now(),
                    type: EventLogMessageType.Attack,
                };

                const playerDefendsMessage = {
                    message: [
                        { text: target.name, color: Colors.EnemyRed },
                        { text: " dealt " },
                        { text: potentialDamage.toFixed(2), color: Colors.PlayerGreen },
                        { text: " damage. (Blocked " },
                        { text: blockedDamage.toFixed(2), color: Colors.PlayerGreen },
                        { text: " damage)" },
                    ],
                    timestamp: Date.now(),
                    type: EventLogMessageType.Attack,
                };

                if (target.speed <= playerSpeed) {
                    await target?.takeDamage(playerDamage);
                    messages.push(playerAttacksMessage);

                    if (target.hp > 0) {
                        playerDoc.hp -= potentialDamage;
                        messages.push(playerDefendsMessage);
                    }
                } else {
                    playerDoc.hp -= potentialDamage;
                    messages.push(playerDefendsMessage);

                    if (playerDoc.hp > 0) {
                        await target?.takeDamage(playerDamage);
                        messages.push(playerAttacksMessage);
                    }
                }

                if (target?.hp <= 0) {
                    floor.spawnItem(target.position, 0.3);

                    floor.removeEnemy(target.uuid);
                    EnemyManager.delete(target.uuid);

                    const gainedExp = target.damage / 2;
                    await playerDoc.gainExp(gainedExp);
                    messages.push({
                        message: [
                            { text: playerDoc.name, color: Colors.UncommonBlue },
                            { text: " killed " },
                            { text: target.name, color: Colors.EnemyRed },
                            { text: " and gained " },
                            { text: (target.damage / 2).toFixed(2), color: Colors.PlayerGreen },
                            { text: " exp." },
                        ],
                        timestamp: Date.now(),
                        type: EventLogMessageType.Attack,
                    });
                }

                if (playerDoc.hp <= 0) {
                    messages.push({
                        message: [
                            { text: playerDoc.name, color: Colors.UncommonBlue },
                            { text: " was killed by " },
                            { text: target.name, color: Colors.EnemyRed },
                        ],
                        timestamp: Date.now(),
                        type: EventLogMessageType.Attack,
                    });

                    floor.enemies.forEach(enemyUuid => {
                        const enemy = EnemyManager.get(enemyUuid)!;
                        if (enemy.targetCharUuid === character.uuid) {
                            enemy.targetCharUuid = undefined;
                        }
                    });

                    await RecordManager.create(user.uuid);
                    await world.removePlayer(playerDoc.uuid);
                }

                await playerDoc.save();
            }
        }

    switch (input) {
        case InputTypes.INPUT_ACTION:
            const { z, x, y } = character.position;

            const item = world.getItemsByPosition({ z, x, y })[0];

            if (item) {
                await ItemDocManager.createAndInsert(item, playerDoc.uuid);
                world.removeItem(item.uuid);
            }

            break;
    }

    Server.io.to(floor.uuid).emit(SMsg.MAP_UPDATE_RES, {
        success: true,
        characters: await floor?.getResolvedCharacters().map(el => el.getInfo()),
        items: await floor?.getResolvedItems().map(el => el.getInfo()),
    });

    return {
        success: true,
        user: await (await UserManager.get(uuid))?.getInfo(),
        floorCount: floor?.floorCount,
        messages,
        timestamp: Date.now(),
    };
};
