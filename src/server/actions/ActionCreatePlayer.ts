import { PlayerManager, Server, UserManager } from "../server_internal";

export default async (data: any) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { name } = data;

    if (!name) {
        return {
            success: false,
            message: "Bad data.",
        };
    }

    const { uuid } = data.auth;
    const userDoc = await UserManager.get(uuid);

    if (userDoc?.player) {
        return {
            success: false,
            message: "User already has a player.",
        };
    }

    const playerDoc = await PlayerManager.create(uuid, name);

    if (playerDoc) {
        const newUserDoc = await UserManager.get(uuid);

        return {
            success: true,
            user: await newUserDoc?.getInfo(),
        };
    }

    return {
        success: false,
        message: "Failed to create player.",
    };
};
