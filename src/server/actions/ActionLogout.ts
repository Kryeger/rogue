import { Socket } from "socket.io";
import { SMsg } from "../../shared/interfaces/SMsg";
import { CharacterManager, Server, UserManager, WorldManager } from "../server_internal";

export default async (data: any, socket: Socket) => {
    if (!Server.checkAuth(data)) {
        return {
            success: true,
            message: "User session not found, logged out anyway.",
        };
    }

    const { uuid } = data.auth;

    const userDoc = await UserManager.get(uuid);
    if (!userDoc) return { success: true };

    const playerDoc = await userDoc?.getPlayer();
    if (!playerDoc || !playerDoc.worldUuid || !playerDoc.charUuid) return { success: true };

    const world = await WorldManager.get(playerDoc.worldUuid);
    if (!world) return { success: true };

    const character = await CharacterManager.get(playerDoc.charUuid);
    if (!character) return { success: true };

    const floor = world.gameMap.floors[character.position.z];

    Server.io.to(floor.uuid).emit(SMsg.MAP_UPDATE_RES, {
        success: true,
        characters: await floor.getResolvedCharacters().map(el => el.getInfo()),
    });

    await world.removePlayer(playerDoc.uuid);

    playerDoc.worldUuid = undefined;
    playerDoc.charUuid = undefined;
    await playerDoc.save();

    socket.leave(floor.uuid);

    const index = Server.secrets.findIndex(el => el.uuid === uuid);

    if (index) {
        Server.secrets.splice(index, 1);
    }

    return {
        success: true,
    };
};
