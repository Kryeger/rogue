import RecordManager from "../managers/RecordManager";
import { inspect } from "../utils/Utils";

export default async (data: any) => {
    return {
        success: true,
        records: await RecordManager.getTop50(),
    };
};
