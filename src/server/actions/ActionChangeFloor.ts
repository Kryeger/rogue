import { Socket } from "socket.io";
import { CHANGE_FLOOR_DIRECTION } from "../../shared/interfaces/ChangeFloorDirection";
import { Maybe } from "../../shared/interfaces/Maybe";
import { SMsg } from "../../shared/interfaces/SMsg";
import { CharacterManager, Server, UserManager, WorldManager } from "../server_internal";
import { inspect } from "../utils/Utils";

//TODO: fix level changes to wrong level

export default async (data: any, socket?: Socket) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { uuid } = data.auth;

    const player = await (await UserManager.get(uuid))?.getPlayer();

    if (!player) {
        return {
            success: false,
            message: "Player not found.",
        };
    }

    if (!player.worldUuid || !player.charUuid) {
        return {
            success: false,
            message: "Player missing data.",
        };
    }

    const world = WorldManager.get(player.worldUuid);

    if (!world) {
        return {
            success: false,
            message: "World not found",
        };
    }

    const character = CharacterManager.get(player.charUuid);

    if (!character) {
        return {
            success: false,
            message: "Character not found",
        };
    }

    const { z, x, y } = character.position;

    const floor = world.gameMap.floors[character.position.z];

    let success;
    let targetFloor: Maybe<number>;

    if (floor.leavePosition.x === x && floor.leavePosition.y) {
        targetFloor = z + 1;
        success = await world.moveToFloor(player.uuid, CHANGE_FLOOR_DIRECTION.UP);

        if (success) {
            player.maxFloor[floor.difficulty]++;
            await player.save();
        }
    } else if (floor.spawnPosition.x === x && floor.spawnPosition.y === y && z !== 0) {
        targetFloor = z - 1;
        success = await world.moveToFloor(player.uuid, CHANGE_FLOOR_DIRECTION.DOWN);
    }

    if (success) {
        const targetFloorUuid = world.gameMap.floors[targetFloor!]?.uuid;

        socket?.leave(floor.uuid);
        socket?.join(targetFloorUuid);

        Server.io.to(floor.uuid).emit(SMsg.MAP_UPDATE_RES, {
            success: true,
            characters: await floor.getResolvedCharacters().map(el => el.getInfo()),
        });

        Server.io.to(targetFloorUuid).emit(SMsg.MAP_UPDATE_RES, {
            success: true,
            characters: await world.gameMap.floors[targetFloor!]
                ?.getResolvedCharacters()
                .map(el => el.getInfo()),
        });

        return {
            success: true,
            floor: await world.gameMap.floors[targetFloor!]?.getInfo(),
            user: await (await UserManager.get(uuid))?.getInfo(),
        };
    }

    return {
        success: false,
        message: "Couldn't change floors.",
    };
};
