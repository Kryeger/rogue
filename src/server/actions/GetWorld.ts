import { CharacterManager, Server, UserManager, WorldManager } from "../server_internal";

export default async (data: any) => {
    if (!Server.checkAuth(data)) {
        return {
            success: false,
            message: "Bad auth.",
        };
    }

    const { worldUuid } = data;

    if (!worldUuid) {
        return {
            success: false,
            message: "Bad data.",
        };
    }

    const { uuid } = data.auth;
    const userDocPop = await (await UserManager.get(uuid))?.populateAll();

    if (!userDocPop) {
        return {
            success: false,
            message: "User not found.",
        };
    }

    if (!userDocPop.player) {
        return {
            success: false,
            message: "Player not found.",
        };
    }

    if (userDocPop.player.hp <= 0) {
        return {
            success: false,
            message: "Player is dead.",
        };
    }

    if (await WorldManager.checkPlayerInWorld(userDocPop.player.uuid, worldUuid)) {
        const world = WorldManager.get(worldUuid);

        return {
            success: true,
            worldLabel: await world?.getLabelInfo(),
            floor: await world?.gameMap.floors[0].getInfo(),
            user: await userDocPop.getInfo(),
        };
    }

    return {
        success: false,
        message: "Couldn't get world info.",
    };
};
