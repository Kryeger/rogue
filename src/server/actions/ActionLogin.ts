import { Socket } from "socket.io";
import { Server, UserManager } from "../server_internal";

export default async (data: any, socket: Socket) => {
    const { username, password } = data;

    if (!username || !password) {
        return {
            success: false,
            message: "Bad data.",
        };
    }

    const foundUser = await UserManager.login(username, password);

    if (foundUser) {
        const user = await foundUser?.getInfo();
        const auth = Server.setAuth(user.uuid, socket);
        return {
            success: true,
            user,
            auth,
        };
    }

    return {
        success: false,
        message: "Wrong credentials.",
    };
};
