import { Socket } from "socket.io";
import { SMsg } from "../../shared/interfaces/SMsg";
import { CharacterManager, Server, UserManager, WorldManager } from "../server_internal";

export default async (data: any, socket: Socket) => {
    const secret = Server.secrets.find(el => el.socketId === socket.id);

    if (!secret) return;

    secret.disconnectTimeout = setTimeout(async () => {
        const userDoc = await UserManager.get(secret.uuid);
        if (!userDoc) return;

        const playerDoc = await userDoc?.getPlayer();
        if (!playerDoc || !playerDoc.worldUuid || !playerDoc.charUuid) return;

        const world = await WorldManager.get(playerDoc.worldUuid);
        if (!world) return;

        const character = await CharacterManager.get(playerDoc.charUuid);
        if (!character) return;

        const floor = world.gameMap.floors[character.position.z];

        socket.leave(floor.uuid);

        await world.removePlayer(playerDoc.uuid);

        Server.io.to(floor.uuid).emit(SMsg.MAP_UPDATE_RES, {
            success: true,
            characters: await floor.getResolvedCharacters().map(el => el.getInfo()),
        });

    }, 5000);

};
