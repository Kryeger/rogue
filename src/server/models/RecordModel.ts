import * as mongoose from "mongoose";
import { Document, model, Schema } from "mongoose";
import { WorldDifficulty } from "../../shared/interfaces/WorldDifficulty";

const RecordSchema = new Schema({
    uuid: {
        type: String,
        required: true,
    },

    userName: {
        type: String,
        required: true,
    },

    playerName: {
        type: String,
        required: true,
    },

    maxFloor: {
        [WorldDifficulty.Easy]: {
            type: Number,
            default: 0,
        },
        [WorldDifficulty.Medium]: {
            type: Number,
            default: 0,
        },
        [WorldDifficulty.Hard]: {
            type: Number,
            default: 0,
        },
        [WorldDifficulty.Extreme]: {
            type: Number,
            default: 0,
        },
    },
});

export interface RecordMethods {
    getInfo: () => Record;
}

export interface RecordDoc extends Document, RecordMethods {
    uuid: string;
    userName: string;
    playerName: string;
    maxFloor: { [key in WorldDifficulty]: number };
}

export interface Record {
    uuid: string;
    userName: string;
    playerName: string;
    maxFloor: { [key in WorldDifficulty]: number };
}

RecordSchema.methods.getInfo = function (): Record {
    const { userName, playerName, maxFloor, uuid } = this as RecordDoc;
    return { userName, playerName, maxFloor, uuid };
};

const RecordModel = model<RecordDoc>("Record", RecordSchema);

export default RecordModel;
