import * as mongoose from "mongoose";
import { Document, model, Schema } from "mongoose";
import { Maybe } from "../../shared/interfaces/Maybe";
import { Player, PlayerDoc } from "./PlayerModel";

const UserSchema = new Schema({
    uuid: {
        type: String,
        required: true,
    },

    hash: {
        type: String,
        required: true,
    },

    username: {
        type: String,
        required: true,
    },

    player: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Player",
    },
});

export interface UserMethods {
    populateAll: () => Promise<UserDocPop>;
    getInfo: () => Promise<User>;
    getPlayer: () => Promise<PlayerDoc>;
}

export interface UserDoc extends Document, UserMethods {
    uuid: string;
    hash: string;
    username: string;
    player: Maybe<mongoose.Schema.Types.ObjectId | PlayerDoc>;
}

export interface UserDocPop extends UserDoc, UserMethods {
    player: Maybe<PlayerDoc>;
}

export interface User {
    uuid: string;
    hash: string;
    username: string;
    player: Maybe<Player>;
}

UserSchema.methods.populateAll = async function (): Promise<UserDocPop> {
    return (await (this as UserDoc).populate("player").execPopulate()) as UserDocPop;
};

UserSchema.methods.getInfo = async function (): Promise<User> {
    const userDocPop = await (this as UserDoc).populateAll();
    const playerDoc = await userDocPop.player;

    const { uuid, hash, username } = userDocPop;
    return { uuid, hash, username, player: await playerDoc?.getInfo() };
};

UserSchema.methods.getPlayer = async function (): Promise<PlayerDoc> {
    const userDoc = await (this as UserDoc).populate("player").execPopulate();
    return userDoc.player as PlayerDoc;
};

const UserModel = model<UserDoc>("User", UserSchema);

export default UserModel;
