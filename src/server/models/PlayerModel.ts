import * as mongoose from "mongoose";
import { Document, model, Schema } from "mongoose";
import { ItemAttribute } from "../../shared/interfaces/ItemAttributes";
import { ItemType } from "../../shared/interfaces/ItemTypes";
import { Maybe } from "../../shared/interfaces/Maybe";
import { WorldDifficulty } from "../../shared/interfaces/WorldDifficulty";
import { ItemDoc, ItemDocManager, ItemInfo, ItemManager } from "../server_internal";

const PlayerSchema = new Schema(
    {
        uuid: {
            type: String,
            required: true,
        },

        worldUuid: {
            type: String,
        },

        userUuid: {
            type: String,
            required: true,
        },

        charUuid: {
            type: String,
        },

        inventory: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Item",
            },
        ],

        level: {
            type: Number,
            default: 1,
        },

        exp: {
            type: Number,
            default: 0,
        },

        toNextLevel: {
            type: Number,
            default: 100,
        },

        hp: {
            type: Number,
            default: 100,
        },

        maxHp: {
            type: Number,
            default: 100,
        },

        gold: {
            type: Number,
            default: 50,
        },

        maxFloor: {
            [WorldDifficulty.Easy]: {
                type: Number,
                default: 0,
            },
            [WorldDifficulty.Medium]: {
                type: Number,
                default: 0,
            },
            [WorldDifficulty.Hard]: {
                type: Number,
                default: 0,
            },
            [WorldDifficulty.Extreme]: {
                type: Number,
                default: 0,
            },
        },

        equipment: {
            helmet: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Item",
            },
            chestplate: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Item",
            },
            pants: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Item",
            },
            boots: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Item",
            },
            weapon: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Item",
            },
        },

        name: {
            type: String,
            required: true,
        },
    },
    { minimize: false }
);

export interface PlayerMethods {
    getInfo: () => Promise<Player>;
    populateAll: () => Promise<PlayerDocPop>;
    equip: (itemUuid: string) => Promise<boolean>;
    unequip: (itemUuid: string) => Promise<boolean>;
    itemIsEquipped: (itemUuid: string) => Promise<boolean>;
    itemIsInInventory: (itemUuid: string) => Promise<boolean>;
    insertItem: (itemDoc: ItemDoc) => Promise<void>;
    removeItem: (itemDoc: ItemDoc) => Promise<boolean>;
    gainExp: (exp: number) => Promise<void>;
    getDamage: () => Promise<number>;
    addGold: (amount: number) => Promise<boolean>;
    removeGold: (amount: number) => Promise<boolean>;
    getEquipment: () => Promise<Array<ItemDoc>>;
    getSpeed: () => Promise<number>;
    getDefense: () => Promise<number>;
}

export interface PlayerDoc extends Document, PlayerMethods {
    uuid: string;
    userUuid: string;
    worldUuid: Maybe<string>;
    charUuid: Maybe<string>;
    name: string;
    level: number;
    exp: number;
    toNextLevel: number;
    hp: number;
    maxHp: number;
    gold: number;
    maxFloor: { [key in WorldDifficulty]: number };
    inventory: Array<mongoose.Schema.Types.ObjectId | ItemDoc>;
    equipment: {
        helmet: Maybe<mongoose.Schema.Types.ObjectId | ItemDoc>;
        chestplate: Maybe<mongoose.Schema.Types.ObjectId | ItemDoc>;
        pants: Maybe<mongoose.Schema.Types.ObjectId | ItemDoc>;
        boots: Maybe<mongoose.Schema.Types.ObjectId | ItemDoc>;
        weapon: Maybe<mongoose.Schema.Types.ObjectId | ItemDoc>;
    };
}

export interface PlayerDocPop extends PlayerDoc {
    inventory: Array<ItemDoc>;
    equipment: {
        helmet: Maybe<ItemDoc>;
        chestplate: Maybe<ItemDoc>;
        pants: Maybe<ItemDoc>;
        boots: Maybe<ItemDoc>;
        weapon: Maybe<ItemDoc>;
    };
}

export interface Player {
    uuid: string;
    userUuid: string;
    worldUuid: Maybe<string>;
    charUuid: Maybe<string>;
    level: number;
    exp: number;
    toNextLevel: number;
    hp: number;
    maxHp: number;
    gold: number;
    maxFloor: { [key in WorldDifficulty]: number };
    inventory: Array<ItemInfo>;
    equipment: {
        helmet: Maybe<ItemInfo>;
        chestplate: Maybe<ItemInfo>;
        pants: Maybe<ItemInfo>;
        boots: Maybe<ItemInfo>;
        weapon: Maybe<ItemInfo>;
    };
    name: string;
}

PlayerSchema.methods.populateAll = async function (): Promise<PlayerDocPop> {
    return (await (this as PlayerDoc)
        .populate("inventory")
        .populate("equipment.helmet")
        .populate("equipment.chestplate")
        .populate("equipment.pants")
        .populate("equipment.boots")
        .populate("equipment.weapon")
        .execPopulate()) as PlayerDocPop;
};

PlayerSchema.methods.getInfo = async function (): Promise<Player> {
    const playerDoc = await (this as PlayerDoc).populateAll();

    const {
        uuid,
        userUuid,
        worldUuid,
        charUuid,
        inventory,
        name,
        equipment,
        level,
        exp,
        toNextLevel,
        hp,
        maxHp,
        gold,
        maxFloor,
    } = playerDoc;

    const { helmet, chestplate, pants, boots, weapon } = equipment;

    //TODO: some cleanup, maybe a function that resolves the item docs.

    return {
        uuid,
        userUuid,
        worldUuid,
        charUuid,
        level,
        exp,
        toNextLevel,
        hp,
        maxHp,
        gold,
        maxFloor,
        inventory: inventory
            .map(el => ItemManager.get(el.uuid) || ItemManager.instantiate(el))
            .map(item => item.getInfo()),
        equipment: {
            helmet:
                helmet &&
                (ItemManager.get(helmet.uuid) || ItemManager.instantiate(helmet)).getInfo(),
            chestplate:
                chestplate &&
                (ItemManager.get(chestplate.uuid) || ItemManager.instantiate(chestplate)).getInfo(),
            pants:
                pants && (ItemManager.get(pants.uuid) || ItemManager.instantiate(pants)).getInfo(),
            boots:
                boots && (ItemManager.get(boots.uuid) || ItemManager.instantiate(boots)).getInfo(),
            weapon:
                weapon &&
                (ItemManager.get(weapon.uuid) || ItemManager.instantiate(weapon)).getInfo(),
        },
        name,
    };
};

PlayerSchema.methods.equip = async function (itemUuid: string) {
    const playerDoc = this as PlayerDoc;

    const playerDocPop = await playerDoc.getInfo();
    if (!playerDocPop) return false;

    const isInInventory = playerDocPop.inventory.find(item => item.uuid === itemUuid);
    if (!isInInventory) return false;

    const itemDoc = await ItemDocManager.get(itemUuid);
    if (!itemDoc) return false;

    switch (itemDoc.itemType) {
        case ItemType.Helmet:
            playerDoc.set("equipment.helmet", itemDoc._id);
            break;

        case ItemType.Chestplate:
            playerDoc.set("equipment.chestplate", itemDoc._id);
            break;

        case ItemType.Pants:
            playerDoc.set("equipment.pants", itemDoc._id);
            break;

        case ItemType.Boots:
            playerDoc.set("equipment.boots", itemDoc._id);
            break;

        case ItemType.Sword:
        case ItemType.Knife:
            playerDoc.set("equipment.weapon", itemDoc._id);
            break;

        case ItemType.HealingPotion:
            const healAmount =
                itemDoc.attributes.find(el => el.attribute === ItemAttribute.Heal)?.value || 0;

            playerDoc.hp = Math.min(playerDoc.hp + healAmount, playerDoc.maxHp);

            playerDoc.inventory.splice(playerDoc.inventory.indexOf(itemDoc._id), 1);

            ItemManager.delete(itemDoc.uuid);
            await itemDoc.delete();

            break;

        default:
            return false;
    }

    await playerDoc.save();
    return true;
};

PlayerSchema.methods.unequip = async function (itemUuid: string) {
    const playerDoc = this as PlayerDoc;
    const playerDocPop = await playerDoc?.populateAll();

    const isInInventory = playerDocPop.inventory.find(item => item.uuid === itemUuid);
    if (!isInInventory) return false;

    const isEquiped = await playerDoc.itemIsEquipped(itemUuid);
    if (!isEquiped) return false;

    switch (isInInventory.itemType) {
        case ItemType.Helmet:
            playerDoc.set("equipment.helmet", undefined);
            break;

        case ItemType.Chestplate:
            playerDoc.set("equipment.chestplate", undefined);
            break;

        case ItemType.Pants:
            playerDoc.set("equipment.pants", undefined);
            break;

        case ItemType.Boots:
            playerDoc.set("equipment.boots", undefined);
            break;

        case ItemType.Sword:
        case ItemType.Knife:
            playerDoc.set("equipment.weapon", undefined);
            break;

        default:
            return false;
    }

    await playerDoc.save();
    return true;
};

PlayerSchema.methods.itemIsEquipped = async function (itemUuid: string) {
    const playerDoc = await (this as PlayerDoc)?.populateAll();

    return (
        playerDoc?.equipment?.helmet?.uuid === itemUuid ||
        playerDoc?.equipment?.chestplate?.uuid === itemUuid ||
        playerDoc?.equipment?.pants?.uuid === itemUuid ||
        playerDoc?.equipment?.boots?.uuid === itemUuid ||
        playerDoc?.equipment?.weapon?.uuid === itemUuid
    );
};

PlayerSchema.methods.itemIsInInventory = async function (itemUuid: string) {
    const playerDocPop = await (this as PlayerDoc)?.populateAll();
    return !!playerDocPop?.inventory?.map(itemDoc => itemDoc?.uuid === itemUuid);
};

PlayerSchema.methods.insertItem = async function (itemDoc: ItemDoc) {
    (this as PlayerDoc).inventory.push(itemDoc._id);
    await this.save();
};

PlayerSchema.methods.removeItem = async function (itemDoc: ItemDoc) {
    const playerDoc = this as PlayerDoc;

    await playerDoc.unequip(itemDoc.uuid);

    playerDoc.inventory.splice(playerDoc.inventory.indexOf(itemDoc._id), 1);
    await ItemDocManager.delete(itemDoc.uuid);
    await playerDoc.save();
};

PlayerSchema.methods.gainExp = async function (exp: number) {
    const playerDoc = this as PlayerDoc;
    playerDoc.exp += exp;

    if (playerDoc.exp >= playerDoc.toNextLevel) {
        playerDoc.exp -= playerDoc.toNextLevel;
        playerDoc.level++;
        playerDoc.toNextLevel *= 1.2;
        playerDoc.maxHp *= 1.3;
        playerDoc.hp = playerDoc.maxHp;
    }

    await playerDoc.save();
};

PlayerSchema.methods.getDamage = async function () {
    const playerDoc = this as PlayerDoc;

    const {
        equipment: { weapon },
    } = await playerDoc.populate("equipment.weapon").execPopulate();

    return (
        (weapon as ItemDoc)?.attributes.find(el => el.attribute === ItemAttribute.Damage)?.value ||
        1
    );
};

PlayerSchema.methods.getEquipment = async function () {
    const playerDoc = (await (this as PlayerDoc)
        .populate("equipment.helmet")
        .populate("equipment.chestplate")
        .populate("equipment.pants")
        .populate("equipment.boots")
        .populate("equipment.weapon")
        .execPopulate()) as PlayerDocPop;

    return (
        [
            playerDoc.equipment.weapon,
            playerDoc.equipment.helmet,
            playerDoc.equipment.chestplate,
            playerDoc.equipment.pants,
            playerDoc.equipment.boots,
        ] || []
    ).filter(el => !!el) as Array<ItemDoc>;
};

PlayerSchema.methods.getSpeed = async function () {
    const playerDoc = this as PlayerDoc;

    let totalSpeed = 0;

    const equipment = await playerDoc.getEquipment();
    for (let equippedItem of equipment) {
        if (!equippedItem.attributes.length) continue;

        for (let attr of equippedItem.attributes) {
            if (attr.attribute === ItemAttribute.Speed) {
                totalSpeed += attr.value;
            }
        }
    }
    return totalSpeed;
};

PlayerSchema.methods.getDefense = async function () {
    const playerDoc = this as PlayerDoc;

    let totalSpeed = 0;

    const equipment = await playerDoc.getEquipment();
    for (let equippedItem of equipment) {
        if (!equippedItem.attributes.length) continue;

        for (let attr of equippedItem.attributes) {
            if (attr.attribute === ItemAttribute.Defense) {
                totalSpeed += attr.value;
            }
        }
    }
    return totalSpeed;
};

PlayerSchema.methods.addGold = async function (amount: number) {
    const playerDoc = this as PlayerDoc;
    playerDoc.gold += amount;
    await playerDoc.save();
};

PlayerSchema.methods.removeGold = async function (amount: number) {
    const playerDoc = this as PlayerDoc;

    if (playerDoc.gold < amount) {
        return false;
    }

    playerDoc.gold -= amount;
    await playerDoc.save();
};

const PlayerModel = model<PlayerDoc>("Player", PlayerSchema);

export default PlayerModel;
