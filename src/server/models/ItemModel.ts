import { Schema, Document, model } from "mongoose";
import { Item, ItemManager } from "../server_internal";
import { ItemAttributeInstance } from "../../shared/interfaces/ItemAttributes";
import { ItemType } from "../../shared/interfaces/ItemTypes";

const ItemSchema = new Schema({
    uuid: {
        type: String,
        required: true,
    },

    name: {
        type: String,
        required: true,
    },

    itemType: {
        type: String,
        required: true,
    },

    rarity: {
        type: String,
        required: true,
    },

    rarityFloat: {
        type: Number,
        required: true,
    },

    attributes: {
        type: [
            {
                attribute: {
                    type: String,
                    required: true,
                },
                value: {
                    type: Number,
                    required: true,
                },
            },
        ],
    },
});

export interface ItemMethods {
    getInstance: () => Promise<Item>;
}

export interface ItemDoc extends Document, ItemMethods {
    uuid: string;
    name: string;
    itemType: ItemType;
    rarity: string;
    rarityFloat: number;
    attributes: Array<ItemAttributeInstance>;
}

ItemSchema.methods.getInstance = async function (): Promise<Item> {
    return ItemManager.instantiate(this as ItemDoc);
};

const ItemModel = model<ItemDoc>("Item", ItemSchema);

export default ItemModel;
