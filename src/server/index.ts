import express from "express";
import Bundler from "parcel-bundler";
import socketio from "socket.io";
import mongoose from "mongoose";

console.log("Current environment:", process.env.NODE_ENV);

mongoose
    .connect("mongodb://localhost/rogue", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => console.log("Connected to DB."));

import "./models/";
import { Server, PlayerModel, UserModel } from "./server_internal";

(async () => {
    //Delete the worldUuid, charUuid and floorCount of all Player documents.
    await PlayerModel.updateMany(
        {},
        { $unset: { worldUuid: true, charUuid: true, floorCount: true } }
    ).exec();

    const deadPlayers = await PlayerModel.find({ hp: { $lt: 0 } });
    await Promise.all(
        deadPlayers.map(async playerDoc => {
            const userDoc = await UserModel.find({ uuid: playerDoc.userUuid });
            userDoc[0].player = undefined;

            await userDoc[0].save();
        })
    );

    await PlayerModel.deleteMany({ hp: { $lt: 0 } });
})();

const app = express();

const bundler = new Bundler("index.html");

app.use(bundler.middleware());

const expressApp = app.listen(3000, () => {
    console.log("Listening on 3000.");
});

const io = socketio(expressApp);

const server = new Server(io);

io.on("connection", async socket => {
    console.log("Someone connected, socket id:", socket.id);
    await server.handleSocket(socket);
});
