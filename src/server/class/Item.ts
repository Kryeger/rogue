import { RarityType, RarityTypeInfo, RarityTypes } from "../../shared/interfaces/raritites";
import { ItemCategoryTypes, ItemType, ItemTypes } from "../../shared/interfaces/ItemTypes";
import shortUUID from "short-uuid";

import Chance from "chance";
import { Vector2 } from "../../shared/interfaces/positions";
import { Maybe } from "../../shared/interfaces/Maybe";
import { ItemAttributeInstance, ItemAttributes } from "../../shared/interfaces/ItemAttributes";

export interface ItemInfo {
    uuid: string;
    name: string;
    itemType: ItemType;
    rarity: RarityTypeInfo;
    position?: Vector2;
    attributes: Array<ItemAttributeInstance>;
    price: number;
}

const chance = new Chance();

export default class Item {
    private _itemType: ItemType;
    private _position?: Vector2;
    private _uuid: string;
    private _name: string;
    private _rarityFloat: number;
    private _rarity: RarityType;
    private _attributes: Array<ItemAttributeInstance> = [];
    private _price: number;

    constructor(
        itemType: ItemType,
        rarityFloat: number,
        uuid?: string,
        position?: Vector2,
        attributes?: Array<ItemAttributeInstance>
    ) {
        this._uuid = uuid || shortUUID.generate();
        this._itemType = itemType;
        this._rarityFloat = rarityFloat;
        this._rarity = Item.getRarityType(rarityFloat);
        this._name = Item.generateName(itemType);
        this._position = position;
        this._attributes = attributes || Item.generateItemAttributeInstances(itemType, rarityFloat);
        this._price = Math.floor(
            chance.integer({ min: 1, max: 10 }) +
                this.attributes.reduce((acc, current) => acc + current.value, 0) *
                    RarityTypes[this.rarity].priceBias
        );
    }

    get price(): number {
        return this._price;
    }

    get itemType(): ItemType {
        return this._itemType;
    }

    get position(): Maybe<Vector2> {
        return this._position;
    }

    set position(value: Maybe<Vector2>) {
        this._position = value;
    }

    get uuid(): string {
        return this._uuid;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get rarityFloat(): number {
        return this._rarityFloat;
    }

    get rarity(): RarityType {
        return this._rarity;
    }

    set rarity(value: RarityType) {
        this._rarity = value;
    }

    get attributes(): Array<ItemAttributeInstance> {
        return this._attributes;
    }

    static getRarityType = (rarityFloat: number) => {
        if (rarityFloat < 0.01) return RarityType.Legendary;

        if (rarityFloat < 0.05) return RarityType.Epic;

        if (rarityFloat < 0.1) return RarityType.Rare;

        if (rarityFloat < 0.2) return RarityType.Uncommon;

        return RarityType.Common;
    };

    static generateItemAttributeInstances = (itemType: ItemType, rarityFloat: number) => {
        //
        const possibleAttributes = ItemTypes[itemType].attributes;

        return possibleAttributes.map(attributeModifier => {
            const attributeBias = attributeModifier.bias;
            const rarityBias = RarityTypes[Item.getRarityType(rarityFloat)].statsBias;

            const value = chance.integer({ min: 1, max: 10 }) * attributeBias * rarityBias;

            return { attribute: attributeModifier.attribute, value } as ItemAttributeInstance;
        });
    };

    static generateName = (itemType: ItemType) => {
        const itemTypeInfo = ItemTypes[itemType];
        const modifier = chance.pickone(ItemCategoryTypes[itemTypeInfo.category].nameModifiers);

        return modifier + " " + itemTypeInfo.name;
    };

    getInfo = (): ItemInfo => {
        const { uuid, name, rarity, itemType, position, attributes, price } = this;
        return { uuid, name, rarity: RarityTypes[rarity], position, itemType, attributes, price };
    };
}
