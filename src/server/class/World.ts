import _ from "lodash";
import shortUUID from "short-uuid";
import { CHANGE_FLOOR_DIRECTION } from "../../shared/interfaces/ChangeFloorDirection";
import { Vector3 } from "../../shared/interfaces/positions";
import { GameMap, GameMapInfo, WorldDifficulty } from "../../shared/shared_internal";
import { Player } from "../models";
import { CharacterManager, PlayerManager } from "../server_internal";
import { generateWorldName } from "../utils/Utils";

export interface WorldLabel {
    uuid: string;
    name: string;
    difficulty: WorldDifficulty;
    playerCount: number;
}

export interface WorldInfo {
    uuid: string;
    name: string;
    gameMap: GameMapInfo;
    players: Array<Player>;
}

export default class World {
    private readonly _uuid: string;
    private readonly _name: string;
    private readonly _gameMap: GameMap;
    private readonly _players: Array<string> = [];
    private readonly _difficulty: WorldDifficulty = WorldDifficulty.Easy;

    constructor(width: number, height: number, difficulty?: WorldDifficulty) {
        this._uuid = shortUUID.generate();
        this._name = generateWorldName();
        difficulty && (this._difficulty = difficulty);

        this._gameMap = new GameMap(width, height, this._difficulty);
    }

    get difficulty(): WorldDifficulty {
        return this._difficulty;
    }

    get players(): Array<string> {
        return this._players;
    }

    get name(): string {
        return this._name;
    }

    get uuid(): string {
        return this._uuid;
    }

    get gameMap(): GameMap {
        return this._gameMap;
    }

    insertPlayer = async (playerUuid: string) => {
        this.players.push(playerUuid);
        return await this.gameMap.floors[0].insertPlayer(playerUuid);
    };

    removePlayer = async (playerUuid: string) => {
        const index = this.players.indexOf(playerUuid);
        if (index === -1) return false;

        for (let floor of this.gameMap.floors) {
            const success = await floor.removePlayer(playerUuid);
            if (success) {
                this.players.splice(index, 1);
                return;
            }
        }
    };

    getItemsByPosition = ({ x, y, z }: Vector3) => {
        return this.gameMap.floors[z]?.getItemsByPosition({ x, y }) || [];
    };

    removeItem = (itemUuid: string) => {
        for (let floor of this.gameMap.floors) {
            if (floor.removeItem(itemUuid)) {
                return;
            }
        }
    };

    getLabelInfo = (): WorldLabel => {
        const { uuid, name, difficulty } = this;
        return { uuid, name, difficulty, playerCount: this.players.length };
    };

    moveToFloor = async (playerUuid: string, direction: CHANGE_FLOOR_DIRECTION) => {
        if (!this.players.find(el => el === playerUuid)) return false;

        const player = await (await PlayerManager.get(playerUuid))?.getInfo();
        if (!player || !player.charUuid) return false;

        const character = CharacterManager.get(player.charUuid);
        if (!character) return false;

        if (direction === CHANGE_FLOOR_DIRECTION.DOWN && character?.position.z === 0) return false;

        const currentFloor = character.position.z;
        let targetFloor = currentFloor + 1;

        switch (direction) {
            case CHANGE_FLOOR_DIRECTION.DOWN:
                targetFloor = currentFloor - 1;

                break;

            case CHANGE_FLOOR_DIRECTION.UP:
                targetFloor = currentFloor + 1;

                if (targetFloor >= this.gameMap.floors.length) {
                    this.gameMap.generateFloors(1);
                }

                break;
        }

        await this.gameMap.floors[currentFloor].removePlayer(playerUuid);
        await this.gameMap.floors[targetFloor].insertPlayer(playerUuid);

        return true;
    };

    getInfo = async (): Promise<WorldInfo> => {
        const { uuid, name } = this;

        const gameMap = await this.gameMap.getInfo();

        const { players } = this;

        const resolvedPlayers = (
            await Promise.all(
                (await PlayerManager.getMany(players)).map(playerDoc => playerDoc?.getInfo())
            )
        ).filter(el => !!el) as Array<Player>;

        return { uuid, name, gameMap, players: resolvedPlayers };
    };
}
