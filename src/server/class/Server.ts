import async from "async";
import Chance from "chance";
import SocketIO from "socket.io";
import socketio, { Packet, Socket } from "socket.io";
import { Auth } from "../../shared/interfaces/Auth";
import { Maybe } from "../../shared/interfaces/Maybe";
import { ServerAction } from "../../shared/interfaces/ServerAction";
import { SMsg } from "../../shared/interfaces/SMsg";
import ActionDisconnect from "../actions/ActionDisconnect";
import ActionLogout from "../actions/ActionLogout";
import ActionRespawn from "../actions/ActionRespawn";
import GetRecords from "../actions/GetRecords";
import {
    ActionChangeFloor,
    ActionConnect,
    ActionCreatePlayer,
    ActionDrop,
    ActionEquip,
    ActionInput,
    ActionJoinWorld,
    ActionLogin,
    ActionPick,
    ActionRegister,
    ActionUnequip,
    CharacterManager,
    GetWorld,
    GetWorlds,
    PlayerManager,
    UserManager,
    WorldManager,
} from "../server_internal";
import { inspect } from "../utils/Utils";

const chance = new Chance();

export interface RegisteredAction {
    action: (data: any, socket: Socket) => void;
    responseMsg?: SMsg;
    dontLogResponse?: boolean;
}

export default class Server {
    private static readonly _secrets: Array<Auth> = [];
    private static _io: socketio.Server;
    private readonly _actions = new Map<string, RegisteredAction>();

    constructor(io: socketio.Server) {
        Server._io = io;
        this.registerActions();
    }

    static get io(): SocketIO.Server {
        return this._io;
    }

    static get secrets(): Array<Auth> {
        return this._secrets;
    }

    get actions(): Map<string, RegisteredAction> {
        return this._actions;
    }

    public static checkAuth = (data: any) => {
        const { auth } = data;

        if (!auth?.uuid || !auth?.secret || !auth?.socketId) {
            return false;
        }

        const matchedAuth = Server.secrets.find(
            el => el.uuid === auth.uuid && el.secret === auth.secret
        );

        if (matchedAuth) {
            data.socketId && (matchedAuth.socketId = data.socketId);
            return true;
        }

        return false;
    };

    public static setAuth = (uuid: string, socket: Socket): Maybe<Auth> => {
        const alreadyLoggedIn = Server.secrets.findIndex(el => el.uuid === uuid);

        if (alreadyLoggedIn !== -1) {
            Server.secrets.splice(alreadyLoggedIn, 1);
        }

        const secret = chance.string({ length: 32 });

        const newAuth = { uuid, secret, socketId: socket.id };

        Server.secrets.push(newAuth);

        return newAuth;
    };

    registerActions = () => {
        this.actions.set(SMsg.CONNECT_REQ, {
            action: ActionConnect,
            responseMsg: SMsg.CONNECT_RES,
        });

        this.actions.set(SMsg.REGISTER_REQ, {
            action: ActionRegister,
            responseMsg: SMsg.REGISTER_RES,
        });

        this.actions.set(SMsg.LOGIN_REQ, {
            action: ActionLogin,
            responseMsg: SMsg.LOGIN_RES,
        });

        this.actions.set(SMsg.CREATE_PLAYER_REQ, {
            action: ActionCreatePlayer,
            responseMsg: SMsg.CREATE_PLAYER_RES,
        });

        this.actions.set(SMsg.GET_WORLDS_REQ, {
            action: GetWorlds,
            responseMsg: SMsg.GET_WORLDS_RES,
        });

        this.actions.set(SMsg.GET_WORLD_REQ, {
            action: GetWorld,
            responseMsg: SMsg.JOIN_WORLD_RES,
        });

        this.actions.set(SMsg.JOIN_WORLD_REQ, {
            action: ActionJoinWorld,
            responseMsg: SMsg.JOIN_WORLD_RES,
            dontLogResponse: true,
        });

        this.actions.set(SMsg.INPUT_REQ, {
            action: ActionInput,
            responseMsg: SMsg.MAP_UPDATE_RES,
            dontLogResponse: true,
        });

        this.actions.set(SMsg.PICK_ITEM_REQ, {
            action: ActionPick,
            responseMsg: SMsg.PICK_ITEM_RES,
        });

        this.actions.set(SMsg.EQUIP_ITEM_REQ, {
            action: ActionEquip,
            responseMsg: SMsg.EQUIP_ITEM_RES,
        });

        this.actions.set(SMsg.UNEQUIP_ITEM_REQ, {
            action: ActionUnequip,
            responseMsg: SMsg.UNEQUIP_ITEM_RES,
        });

        this.actions.set(SMsg.DROP_ITEM_REQ, {
            action: ActionDrop,
            responseMsg: SMsg.DROP_ITEM_RES,
        });

        this.actions.set(SMsg.CHANGE_FLOOR_REQ, {
            action: ActionChangeFloor,
            responseMsg: SMsg.CHANGE_FLOOR_RES,
        });

        this.actions.set(SMsg.RESPAWN_REQ, {
            action: ActionRespawn,
            responseMsg: SMsg.RESPAWN_RES,
        });

        this.actions.set(SMsg.GET_RECORDS_REQ, {
            action: GetRecords,
            responseMsg: SMsg.GET_RECORDS_RES,
        });

        this.actions.set(SMsg.DISCONNECT, {
            action: ActionDisconnect,
        });

        this.actions.set(SMsg.LOGOUT_REQ, {
            action: ActionLogout,
            responseMsg: SMsg.LOGOUT_RES,
        });
    };

    dumpPacket = async (packet: Packet, next: (err?: any) => void) => {
        const actionName = packet["0"];
        const actionData = packet["1"];

        process.env.DEBUG == "true" &&
            console.log(`received: [${actionName}, ${inspect(actionData)}]`);

        next();
    };

    handleSocket = async (socket: Socket) => {
        socket.use(this.dumpPacket);

        await async.each(this.actions.keys(), key => {
            socket.on(key, async data => {
                const { action, responseMsg, dontLogResponse } = this.actions.get(key)!; //key is always valid
                const actionResult = await action(data, socket);

                responseMsg && socket.emit(responseMsg, actionResult);
                if (!dontLogResponse)
                    console.log(`emitted: [${responseMsg}, ${inspect(actionResult)}]`);
            });
        });
    };
}
