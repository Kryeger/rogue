import { TileTypeInfo } from "../../shared/TileTypes";

export interface TileInfo {
    tileTypeInfo: TileTypeInfo;
}

//TODO: Redo this.

export default class Tile {
    private _tileTypeInfo: TileTypeInfo;

    constructor(tileInfo: TileTypeInfo) {
        this._tileTypeInfo = tileInfo;
    }

    get tileTypeInfo(): TileTypeInfo {
        return this._tileTypeInfo;
    }

    set tileTypeInfo(value: TileTypeInfo) {
        this._tileTypeInfo = value;
    }

    getInfo(): TileInfo {
        const { tileTypeInfo } = this;
        return { tileTypeInfo };
    }
}
