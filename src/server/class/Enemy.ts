import { Maybe } from "../../shared/interfaces/Maybe";
import { Vector2 } from "../../shared/interfaces/positions";
import shortUUID from "short-uuid";
import Chance from "chance";

const chance = new Chance();

export interface EnemyInfo {
    uuid: string;
    name: string;
    position: Vector2;
    hp: number;
    speed: number;
    targetCharUuid: Maybe<string>;
}

export default class Enemy {
    private _uuid: string;
    private _name: string;
    private _speed: number;
    private _hp: number;
    private _damage: number;
    private _targetCharUuid: Maybe<string>;
    private _timeToForget = 100;
    private _position: Vector2;

    constructor(name: string, level: number, position: Vector2, uuid?: string) {
        this._uuid = uuid || shortUUID().generate();
        this._name = name;
        this._damage = level * 10 * chance.floating({ min: level, max: level + 1 });
        this._hp = Math.ceil(level * chance.floating({ min: 1, max: 5 }));
        this._speed = level * chance.floating({ min: 0.5, max: 2 });
        this._position = position;
    }

    get timeToForget(): number {
        return this._timeToForget;
    }

    set timeToForget(value: number) {
        this._timeToForget = value;
    }

    get speed(): number {
        return this._speed;
    }

    get hp(): number {
        return this._hp;
    }

    set hp(value: number) {
        this._hp = value;
    }

    get uuid(): string {
        return this._uuid;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get damage(): number {
        return this._damage;
    }

    get targetCharUuid(): Maybe<string> {
        return this._targetCharUuid;
    }

    set targetCharUuid(value: Maybe<string>) {
        this._targetCharUuid = value;
    }

    get position(): Vector2 {
        return this._position;
    }

    set position(value: Vector2) {
        this._position = value;
    }

    takeDamage = (value: number) => {
        this.hp -= value;
    };

    getInfo(): EnemyInfo {
        const { uuid, name, position, targetCharUuid, hp, speed } = this;
        return { uuid, name, position, targetCharUuid, hp, speed };
    }
}
