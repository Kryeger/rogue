import { Vector3 } from "../../shared/interfaces/positions";
import shortUUID from "short-uuid";

export interface CharacterInfo {
    uuid: string;
    position: Vector3;
}

export default class Character {
    private readonly _uuid: string;
    private _position: Vector3;

    constructor({ x, y, z }: Vector3) {
        this._uuid = shortUUID.generate();
        this._position = { x, y, z };
    }

    get uuid(): string {
        return this._uuid;
    }

    get position(): Vector3 {
        return this._position;
    }

    set position(value: Vector3) {
        this._position = value;
    }

    getInfo = (): CharacterInfo => {
        const { uuid, position } = this;
        return { uuid, position };
    };
}
