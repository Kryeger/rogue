import Chance from "chance";
import * as util from "util";
import { uniqueNamesGenerator, colors, names, Config } from "unique-names-generator";
import { Vector2 } from "../../shared/interfaces/positions";

const nameGenConfig: Config = {
    dictionaries: [colors, names],
};

const chance = new Chance();

export const log = (object: any) => console.log(inspect(object));

export const inspect = (object: any) => util.inspect(object, false, null, true);

export const dumpMap = <K, V>(map: Map<K, V>) => {
    map.forEach((value: any, key: any) => console.log(`${key}: ${value}`));
};

export const generateWorldName = () => uniqueNamesGenerator(nameGenConfig);

export const isInInterval = (
    numberToCheck: number,
    min: number,
    max: number,
    closed: boolean = true
) =>
    closed
        ? numberToCheck >= min && numberToCheck <= max
        : numberToCheck > min && numberToCheck < max;

export const diagonalDistance = (a: Vector2, b: Vector2) => {
    let dx = b.x - a.x,
        dy = b.y - a.y;
    return Math.max(Math.abs(dx), Math.abs(dy));
};

export const roundPoint = (a: Vector2) => {
    return { x: Math.round(a.x), y: Math.round(a.y) };
};

export const lerpPoint = (a: Vector2, b: Vector2, t: number) => {
    return { x: lerp(a.x, b.x, t), y: lerp(a.y, b.y, t) };
};

export const lerp = (start: number, end: number, t: number) => {
    return start + t * (end - start);
};

export const raycast = (a: Vector2, b: Vector2) => {
    let points = [];
    let N = diagonalDistance(a, b);

    for (let step = 0; step <= N; step++) {
        let t = N === 0 ? 0.0 : step / N;
        points.push(roundPoint(lerpPoint(a, b, t)));
    }

    return points;
};

export const doIfChance = <T>(chanceToDo: number, f: () => T) => {
    if (chance.floating({ min: 0, max: 1 }) < chanceToDo) return f();
};
