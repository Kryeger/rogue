import _ from "lodash";
import shortUUID from "short-uuid";
import {
    CharacterManager,
    ItemDocManager,
    ItemManager,
    PlayerModel,
    PlayerDoc,
    UserManager,
    WorldManager,
} from "../server_internal";
import { Maybe } from "../../shared/interfaces/Maybe";

export default class PlayerManager {
    static create = async (userUuid: string, name: string): Promise<Maybe<PlayerDoc>> => {
        const userDoc = await UserManager.get(userUuid);
        if (!userDoc) return;

        const uuid = shortUUID.generate();

        const playerDoc = new PlayerModel({
            uuid,
            userUuid,
            name,
        });

        await playerDoc.save();

        userDoc.player = playerDoc._id;
        await userDoc.save();

        return playerDoc;
    };

    static drop = async (playerUuid: string, itemUuid: string) => {
        const [playerDoc, itemDoc] = await Promise.all([
            PlayerManager.get(playerUuid),
            ItemDocManager.get(itemUuid),
        ]);

        if (!playerDoc || !playerDoc.worldUuid || !playerDoc.charUuid || !itemDoc) return false;

        const world = WorldManager.get(playerDoc.worldUuid);
        const character = CharacterManager.get(playerDoc.charUuid);
        if (!world || !character) return false;

        await playerDoc.removeItem(itemDoc);

        const alreadyInstantiated = ItemManager.get(itemDoc.uuid);

        if (alreadyInstantiated) {
            alreadyInstantiated.position = _.clone(character.position);
            world.gameMap.floors[character.position.z].insertItem(alreadyInstantiated.uuid);
        } else {
            const item = ItemManager.instantiate(itemDoc, _.clone(character.position));
            world.gameMap.floors[character.position.z].insertItem(item.uuid);
        }

        return true;
    };

    static get = async (uuid: string) => {
        return PlayerModel.findOne({ uuid }).exec();
    };

    static getMany = async (uuids: Array<string>) => {
        return PlayerModel.find({ uuid: { $in: uuids } }).exec();
    };

    static delete = async (uuid: string) => PlayerModel.deleteOne({ uuid });
}
