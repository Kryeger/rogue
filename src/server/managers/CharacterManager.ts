import { Character } from "../server_internal";

export default class CharacterManager {
    static instances = new Map<string, Character>();

    static create = async (z: number, x: number, y: number): Promise<Character> => {
        const character = new Character({ z, x, y });
        CharacterManager.instances.set(character.uuid, character);
        return character;
    };

    static delete = (uuid: string) => {
        return CharacterManager.instances.delete(uuid);
    };

    static get = (uuid?: string) => CharacterManager.instances.get(uuid || "");
}
