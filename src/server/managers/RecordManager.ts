import { WorldDifficulty } from "../../shared/interfaces/WorldDifficulty";
import { RecordDoc, RecordModel, UserManager } from "../server_internal";
import shortUUID from "short-uuid";

export default class RecordManager {
    static create = async (userUuid: string) => {
        const user = await (await UserManager.get(userUuid))?.getInfo();

        if (!user || !user.player) return;

        const newRecordDoc = new RecordModel({
            uuid: shortUUID.generate(),
            userName: user.username,
            playerName: user.player.name,
            maxFloor: user.player.maxFloor,
        });

        await newRecordDoc.save();

        return newRecordDoc;
    };

    static get = async (uuid: string) => await RecordModel.findOne({ uuid }).exec();

    static getTop50 = async () => {
        const records = (await RecordModel.find()) as Array<RecordDoc>;

        return records?.map(recordDoc => recordDoc.getInfo());
    };
}
