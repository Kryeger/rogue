import Chance from "chance";
import { Vector2 } from "../../shared/interfaces/positions";
import { Maybe } from "../../shared/interfaces/Maybe";
import { Enemy } from "../server_internal";
import { WorldDifficulty } from "../../shared/shared_internal";

const chance = new Chance();

export default class EnemyManager {
    static instances = new Map<string, Enemy>();

    static create = (
        name: string,
        level: number,
        difficulty: WorldDifficulty,
        position: Vector2
    ): Enemy => {
        const enemy = new Enemy(name, level, position);
        EnemyManager.instances.set(enemy.uuid, enemy);
        return enemy;
    };

    static createByChance = (
        name: string,
        level: number,
        difficulty: WorldDifficulty,
        position: Vector2,
        chanceToSpawn: number
    ): Maybe<Enemy> => {
        if (chance.floating({ min: 0, max: 1 }) < chanceToSpawn) {
            return EnemyManager.create(name, level, difficulty, position);
        }
    };

    static get = (uuid: string) => EnemyManager.instances.get(uuid);

    static delete = (uuid: string) => EnemyManager.instances.delete(uuid);
}
