import Chance from "chance";
import { Item, ItemDoc } from "../server_internal";
import { ItemType } from "../../shared/interfaces/ItemTypes";
import { Maybe } from "../../shared/interfaces/Maybe";
import { Vector2 } from "../../shared/interfaces/positions";
import { WorldDifficulty } from "../../shared/shared_internal";

const chance = new Chance();

export default class ItemManager {
    static instances = new Map<string, Item>();

    static createByChance = (
        position: Vector2,
        chanceToSpawn: number,
        difficulty: WorldDifficulty
    ): Maybe<Item> => {
        if (chance.floating({ min: 0, max: 1 }) < chanceToSpawn) {
            return ItemManager.create(position, difficulty);
        }
    };

    static instantiate = (itemDoc: ItemDoc, position?: Vector2) => {
        const item = new Item(
            itemDoc.itemType as ItemType,
            itemDoc.rarityFloat,
            itemDoc.uuid,
            position,
            itemDoc.attributes
        );
        ItemManager.instances.set(itemDoc.uuid, item);
        return item;
    };

    static create = (position: Vector2, difficulty: WorldDifficulty, itemType?: ItemType) => {
        let maxRarityFloat = 1;

        if (difficulty === WorldDifficulty.Medium) {
            maxRarityFloat = 0.8;
        } else if (difficulty === WorldDifficulty.Hard) {
            maxRarityFloat = 0.6;
        } else if (difficulty === WorldDifficulty.Extreme) {
            maxRarityFloat = 0.4;
        }

        const itemRarityFloat = chance.floating({ min: 0, max: maxRarityFloat });

        if (!itemType) itemType = chance.pickone(Object.values(ItemType));

        const newItem = new Item(itemType, itemRarityFloat, undefined, position);

        ItemManager.instances.set(newItem.uuid, newItem);

        return newItem;
    };

    static delete = (uuid: string) => ItemManager.instances.delete(uuid);

    static get = (uuid: string) => ItemManager.instances.get(uuid);
}
