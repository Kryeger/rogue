import { Item, ItemModel, PlayerManager } from "../server_internal";

export default class ItemDocManager {
    static createAndInsert = async (item: Item, playerUuid: string) => {
        const { uuid, name, itemType, rarity, rarityFloat, attributes } = item;

        const playerDoc = await PlayerManager.get(playerUuid);

        if (!playerDoc) return;

        const itemDoc = new ItemModel({
            uuid,
            name,
            itemType,
            rarityFloat,
            rarity,
            attributes,
        });

        await itemDoc.save();

        await playerDoc.insertItem(itemDoc);

        return itemDoc;
    };
    static get = async (uuid: string) => {
        return ItemModel.findOne({ uuid }).exec();
    };

    static delete = async (uuid: string) => {
        return await ItemModel.deleteOne({ uuid }).exec();
    };
}
