import _ from "lodash";
import { WorldDifficulty } from "../../shared/shared_internal";
import { PlayerManager, World } from "../server_internal";

export default class WorldManager {
    static get instances(): Map<string, World> {
        return this._instances;
    }

    private static _instances = new Map<string, World>();

    static staticConstructor() {
        WorldManager.create(70, 30, WorldDifficulty.Easy);
        WorldManager.create(70, 30, WorldDifficulty.Medium);
        WorldManager.create(70, 30, WorldDifficulty.Hard);
        WorldManager.create(70, 30, WorldDifficulty.Extreme);
    }

    static create = async (
        width: number,
        height: number,
        difficulty?: WorldDifficulty
    ): Promise<World> => {
        const newWorld = new World(width, height, difficulty);

        WorldManager.instances.set(newWorld.uuid, newWorld);

        return newWorld;
    };

    static insertPlayer = async (worldUuid: string, playerUuid: string): Promise<boolean> => {
        const world = WorldManager.get(worldUuid);
        if (!world) return false;

        const playerDoc = await PlayerManager.get(playerUuid);
        if (!playerDoc) return false;

        const newCharUuid = await world.insertPlayer(playerUuid);

        if (!newCharUuid) return false;

        playerDoc.worldUuid = world.uuid;
        playerDoc.charUuid = newCharUuid;

        await playerDoc.save();
        return true;
    };

    static checkPlayerInWorld = async (playerUuid: string, worldUuid: string) => {
        const world = WorldManager.get(worldUuid);
        if (!world) return false;

        return !!world.players?.find(uuid => uuid === playerUuid);
    };

    static get = (uuid: string) => {
        return WorldManager.instances.get(uuid);
    };
}

WorldManager.staticConstructor();
