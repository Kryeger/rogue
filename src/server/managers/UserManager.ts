import { Maybe } from "../../shared/interfaces/Maybe";
import bcrypt from "bcrypt";
import shortUUID from "short-uuid";
import { UserModel, UserDoc } from "../server_internal";

export default class UserManager {
    static register = async (username: string, password: string): Promise<Maybe<UserDoc>> => {
        //Check if user with same username
        //TODO: check same email as well

        const found = await UserModel.findOne({ username }).exec();
        if (found) return;

        const hash = await bcrypt.hash(password, 10);
        const uuid = shortUUID.generate(); //TODO: check collision

        const userDoc = new UserModel({
            uuid,
            hash,
            username,
        });

        await userDoc.save();

        return userDoc;
    };

    static login = async (username: string, password: string): Promise<Maybe<UserDoc>> => {
        const found = await UserModel.findOne({ username }).exec();

        if (!found) {
            return;
        }
        const hashCheck = await bcrypt.compare(password, found.hash);

        if (hashCheck) {
            return found;
        }

        return;
    };

    static get = async (uuid: string) => {
        return await UserModel.findOne({ uuid }).exec();
    };
}
